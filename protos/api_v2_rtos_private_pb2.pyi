from protos import api_v2_pb2 as _api_v2_pb2
from google.protobuf.internal import enum_type_wrapper as _enum_type_wrapper
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from typing import ClassVar as _ClassVar, Mapping as _Mapping, Optional as _Optional, Union as _Union

DESCRIPTOR: _descriptor.FileDescriptor

class TunableAccessMode(int, metaclass=_enum_type_wrapper.EnumTypeWrapper):
    __slots__ = []
    TUNABLE_ACCESS_MODE_UNSPECIFIED: _ClassVar[TunableAccessMode]
    TUNABLE_ACCESS_MODE_READ: _ClassVar[TunableAccessMode]
    TUNABLE_ACCESS_MODE_WRITE: _ClassVar[TunableAccessMode]
TUNABLE_ACCESS_MODE_UNSPECIFIED: TunableAccessMode
TUNABLE_ACCESS_MODE_READ: TunableAccessMode
TUNABLE_ACCESS_MODE_WRITE: TunableAccessMode

class TunableRequest(_message.Message):
    __slots__ = ["header", "tunable_path", "value", "access_mode"]
    HEADER_FIELD_NUMBER: _ClassVar[int]
    TUNABLE_PATH_FIELD_NUMBER: _ClassVar[int]
    VALUE_FIELD_NUMBER: _ClassVar[int]
    ACCESS_MODE_FIELD_NUMBER: _ClassVar[int]
    header: _api_v2_pb2.CmdHeader
    tunable_path: str
    value: str
    access_mode: TunableAccessMode
    def __init__(self, header: _Optional[_Union[_api_v2_pb2.CmdHeader, _Mapping]] = ..., tunable_path: _Optional[str] = ..., value: _Optional[str] = ..., access_mode: _Optional[_Union[TunableAccessMode, str]] = ...) -> None: ...

class TunableResponse(_message.Message):
    __slots__ = ["header", "tunable_path", "status_code", "value"]
    HEADER_FIELD_NUMBER: _ClassVar[int]
    TUNABLE_PATH_FIELD_NUMBER: _ClassVar[int]
    STATUS_CODE_FIELD_NUMBER: _ClassVar[int]
    VALUE_FIELD_NUMBER: _ClassVar[int]
    header: _api_v2_pb2.StateHeader
    tunable_path: str
    status_code: int
    value: str
    def __init__(self, header: _Optional[_Union[_api_v2_pb2.StateHeader, _Mapping]] = ..., tunable_path: _Optional[str] = ..., status_code: _Optional[int] = ..., value: _Optional[str] = ...) -> None: ...
