'''
MIT License

Copyright (c) 2024 Fuzzy Logic Robotics

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
'''

import json, requests, time, uuid
from protos import api_v2_rtos_pb2 as flr_rtos_api_v2
from protos import api_v2_pb2 as flr_api_v2
from google.protobuf import json_format
from math import isclose
import urllib.parse


def printTool(tool: flr_rtos_api_v2.Tool):
    print("Tool name:", (tool.name or "none"))
    print(
        "  Translation: [",
        tool.flange_to_tcp.translation.x,
        ",",
        tool.flange_to_tcp.translation.y,
        ",",
        tool.flange_to_tcp.translation.z,
        "]",
    )
    print(
        "  Rotation: [",
        tool.flange_to_tcp.rotation.qw,
        ",",
        tool.flange_to_tcp.rotation.qx,
        ",",
        tool.flange_to_tcp.rotation.qy,
        ",",
        tool.flange_to_tcp.rotation.qz,
        "]",
    )


def handle_response(r):
    if r.status_code != 200:
        raise Exception("Request failed: " + json.dumps(r.json()))
    return r


def buildWaypoint(
    motion_type: flr_rtos_api_v2.MotionType,
    pose: flr_rtos_api_v2.RobotPose = None,
    q: flr_api_v2.Joints = None,
    joint_seeds: flr_api_v2.Joints = None,
    v: int = 100,
    b: int = 0,
):
    wpt = flr_rtos_api_v2.Waypoint(
        blending_percent=max(0, min(b, 100)),
        velocity_scaling_percent=max(0, min(v, 100)),
        motion_type=motion_type,
    )

    if pose:
        wpt.robot_pose.CopyFrom(flr_rtos_api_v2.RobotPose(pose=pose))
    if joint_seeds:
        wpt.robot_pose.joint_seeds.CopyFrom(joint_seeds)
    if q:
        wpt.joint_positions.CopyFrom(q)

    return wpt


class Wpt:
    def Linear(
        pose: flr_rtos_api_v2.RobotPose,
        joint_seeds: flr_api_v2.Joints = None,
        v: int = 100,
        b: int = 0,
    ):
        return buildWaypoint(
            flr_rtos_api_v2.MOTION_TYPE_LIN,
            pose=pose,
            joint_seeds=joint_seeds,
            v=v,
            b=b,
        )

    def Spline(
        pose: flr_rtos_api_v2.RobotPose,
        joint_seeds: flr_api_v2.Joints = None,
        v: int = 100,
        b: int = 0,
    ):
        return buildWaypoint(
            flr_rtos_api_v2.MOTION_TYPE_SPLINE,
            pose=pose,
            joint_seeds=joint_seeds,
            v=v,
            b=b,
        )

    def Joint(
        q: flr_api_v2.Joints,
        v: int = 100,
        b: int = 0,
    ):
        return buildWaypoint(flr_rtos_api_v2.MOTION_TYPE_PTP, q=q, v=v, b=b)


def are_joints_equal(q1: flr_api_v2.Joints, q2: flr_api_v2.Joints):
    for a, b in zip(q1.value, q2.value):
        if not isclose(a, b, abs_tol=1e-5):
            return False
    return True


class FuzzyRtosRuntimeApi:
    def __init__(self, host="localhost", port=8000):
        self.base_url = "http://" + host + ":" + str(port)
        self.session = requests.Session()
        self.session.get(self.base_url)

    def get_version(self):
        return handle_response(self.session.get(self.base_url + "/version")).text

    # License
    def get_license_validity(self):
        return json.loads(
            handle_response(self.session.get(self.base_url + "/license/validity")).text
        )["is_valid"]

    def get_license_name(self):
        return handle_response(self.session.get(self.base_url + "/license/name")).text

    def get_license_key(self):
        return json.loads(
            handle_response(self.session.get(self.base_url + "/license/key")).text
        )["key"]

    # Robots
    class RobotDesc:
        def __init__(self, data):
            self.brand = data["brand"]
            self.model = data["model"]
            self.id = data["id"]

    def get_all_robots(self):
        r = self.session.get(self.base_url + "/robots")
        if r.status_code == 200:
            output = []
            for item in json.loads(r.text)["items"]:
                output.append(self.RobotDesc(item))
            return output
        raise Exception("Request failed: " + r.reason)

    def get_robot_by_id(self, id):
        return self.RobotDesc(
            json.loads(
                handle_response(
                    self.session.get(self.base_url + "/robots/by-id/" + str(id))
                ).text
            )
        )

    # Controllers
    class ControllerDesc:
        def __init__(self, data):
            self.brand = data["brand"]
            self.model = data["model"]
            self.id = data["id"]

    def get_all_controllers(self):
        r = self.session.get(self.base_url + "/controllers")
        if r.status_code == 200:
            output = []
            for item in json.loads(r.text)["items"]:
                output.append(self.ControllerDesc(item))
            return output
        raise Exception("Request failed: " + r.reason)

    def get_controller_by_id(self, id):
        return self.ControllerDesc(
            json.loads(
                handle_response(
                    self.session.get(self.base_url + "/controllers/by-id/" + str(id))
                ).text
            )
        )

    # Setup
    class DriverConf:
        def __init__(self, data):
            self.data = data
            self.controller_id = data["controller_id"]
            self.robot_id = data["robot_id"]
            self.is_dummy_driver = data["dummy_driver"]

    def get_driver_conf(self):
        return self.DriverConf(
            json.loads(
                handle_response(self.session.get(self.base_url + "/driver/conf")).text
            )
        )

    def get_robot(self):
        conf = self.get_driver_conf()

        for r in self.get_all_robots():
            if r.id == conf.robot_id:
                robot_brand = r.brand
                robot_model = r.model
                break

        if robot_brand and robot_model:
            return self.Robot(self.base_url, self.session, robot_brand, robot_model)

        raise Exception("Failed to retrieve current robot model")

    def release_robot(self):
        r = self.session.post(self.base_url + "/driver/stop")

    def configure_robot(
        self,
        brand,
        robot_model,
        controller_model,
        dummy_driver=False,
        local_address=None,
        local_port=None,
        remote_address=None,
        remote_port=None,
    ):
        c_id = None
        r_id = None

        for c in self.get_all_controllers():
            if c.brand == brand and c.model == controller_model:
                c_id = c.id
                break

        for r in self.get_all_robots():
            if r.brand == brand and r.model == robot_model:
                r_id = r.id
                break

        if c_id and r_id:
            handle_response(
                self.session.post(
                    self.base_url + "/driver/start",
                    json={
                        # TODO
                        # "brand": brand,
                        # "robot_model": robot_model,
                        # "controller_model": controller_model,
                        "controller_id": c_id,
                        "robot_id": r_id,
                        "dummy_driver": dummy_driver,
                        "options": {
                            "local_address": local_address,
                            "local_port": local_port,
                            "remote_address": remote_address,
                            "remote_port": remote_port,
                        },
                    },
                )
            )

            return self.get_robot()
        else:
            raise Exception("Robot and/or controller not found")

    class Robot:
        def __init__(self, base_url, session, brand, model):
            self.base_url = base_url
            self.session = session
            self.brand = brand
            self.model = model

        # State
        def get_state(self):
            system_state = flr_rtos_api_v2.SystemState()
            json_format.Parse(
                handle_response(self.session.get(self.base_url + "/driver/state")).text,
                system_state,
                ignore_unknown_fields=False,
                descriptor_pool=None,
                max_recursion_depth=100,
            )
            return system_state

        def get_trajectory_state(self):
            trajectory_state = flr_rtos_api_v2.TrajectoryState()
            json_format.Parse(
                handle_response(
                    self.session.get(self.base_url + "/driver/trajectory/player")
                ).text,
                trajectory_state,
            )
            return trajectory_state

        def get_trajectory_generator_state(self):
            trajectory_generator_state = flr_rtos_api_v2.TrajectoryGeneratorState()
            json_format.Parse(
                handle_response(
                    self.session.get(self.base_url + "/driver/trajectory/generator")
                ).text,
                trajectory_generator_state,
            )
            return trajectory_generator_state

        def set_control_mode_idle(self):
            self.set_control_mode(flr_rtos_api_v2.RTOS_CONTROL_MODE_IDLE)

        def set_control_mode_jogging(self):
            self.set_control_mode(flr_rtos_api_v2.RTOS_CONTROL_MODE_JOGGING)

        def set_control_mode(self, control_mode: flr_rtos_api_v2.RTOSControlMode):
            cm_cmd = flr_rtos_api_v2.RTOSControlModeCmd()
            cm_cmd.control_mode_request = control_mode

            handle_response(
                self.session.post(
                    self.base_url + "/driver/control_mode",
                    json_format.MessageToJson(cm_cmd),
                )
            )

        def set_speed_mode_reduced(self):
            self.set_speed_mode(flr_rtos_api_v2.SPEED_MODE_REDUCED)

        def set_speed_mode_fast(self):
            self.set_speed_mode(flr_rtos_api_v2.SPEED_MODE_FAST)

        def set_speed_mode_max(self):
            self.set_speed_mode(flr_rtos_api_v2.SPEED_MODE_MAX)

        def set_speed_mode(self, speed_mode: flr_rtos_api_v2.SpeedMode):
            sm_cmd = flr_rtos_api_v2.SpeedModeCmd()
            sm_cmd.speed_mode = speed_mode

            handle_response(
                self.session.post(
                    self.base_url + "/driver/speed_mode",
                    json_format.MessageToJson(sm_cmd),
                )
            )

        def set_tool(self, tool: flr_rtos_api_v2.Tool):
            tool_cmd = flr_rtos_api_v2.ToolCmd()
            tool_cmd.tool.CopyFrom(tool)

            handle_response(
                self.session.post(
                    self.base_url + "/driver/tool", json_format.MessageToJson(tool_cmd)
                )
            )

        def enable_recording(self):
            handle_response(
                self.session.post(
                    self.base_url + "/driver/enable_record", json={"value": True}
                )
            )

        def disable_recording(self):
            handle_response(
                self.session.post(
                    self.base_url + "/driver/enable_record", json={"value": False}
                )
            )

        def get_home_position(self):
            q = flr_api_v2.Joints()
            json_format.Parse(
                handle_response(self.session.get(self.base_url + "/driver/home")).text,
                q,
            )
            return q

        def fk(self, q: flr_api_v2.Joints):
            x = flr_api_v2.Pose()

            json_format.Parse(
                handle_response(
                    self.session.post(
                        self.base_url + "/driver/kinematics/forward",
                        json_format.MessageToJson(q),
                    )
                ).text,
                x,
                ignore_unknown_fields=False,
                descriptor_pool=None,
                max_recursion_depth=100,
            )

            return x

        def ik(self, x: flr_api_v2.Pose):
            q = flr_api_v2.Joints()

            json_format.Parse(
                handle_response(
                    self.session.post(
                        self.base_url + "/driver/kinematics/inverse",
                        json_format.MessageToJson(x),
                    )
                ).text,
                q,
                ignore_unknown_fields=False,
                descriptor_pool=None,
                max_recursion_depth=100,
            )

            return q

        # Jogging
        def jog(
            self,
            q: flr_api_v2.Joints = None,
            qd: flr_api_v2.Joints = None,
            x: flr_api_v2.Pose = None,
            xd: flr_api_v2.Twist = None,
        ):
            if q:
                self.joint_position_jog(q)
            if qd:
                self.joint_velocity_jog(qd)
            if x:
                self.cartesian_pose_jog(x)
            if xd:
                self.cartesian_velocity_jog(xd)

        def joint_position_jog(self, q: flr_api_v2.Joints):
            qpj = flr_rtos_api_v2.JointPositionJoggingCmd()
            qpj.joint_positions.CopyFrom(q)

            handle_response(
                self.session.post(
                    self.base_url + "/driver/jogging/joint/position",
                    json_format.MessageToJson(qpj),
                )
            )

        def joint_velocity_jog(self, qd: flr_api_v2.Joints):
            qvj = flr_rtos_api_v2.JointVelocityJoggingCmd()
            qvj.joint_velocities.CopyFrom(qd)

            handle_response(
                self.session.post(
                    self.base_url + "/driver/jogging/joint/velocity",
                    json_format.MessageToJson(qvj),
                )
            )

        def cartesian_pose_jog(self, x: flr_api_v2.Pose):
            cpj = flr_rtos_api_v2.CartesianPoseJoggingCmd()
            cpj.desired_pose.CopyFrom(x)

            handle_response(
                self.session.post(
                    self.base_url + "/driver/jogging/cartesian/pose",
                    json_format.MessageToJson(cpj),
                )
            )

        def cartesian_velocity_jog(self, xd: flr_api_v2.Twist):
            cvj = flr_rtos_api_v2.CartesianVelocityJoggingCmd()
            cvj.desired_velocity.CopyFrom(xd)

            handle_response(
                self.session.post(
                    self.base_url + "/driver/jogging/cartesian/velocity",
                    json_format.MessageToJson(cvj),
                )
            )

        # Trajectories
        def move(
            self,
            wpt: flr_rtos_api_v2.Waypoint = None,
            wpts: list[flr_rtos_api_v2.Waypoint] = None,
        ):
            state = self.get_state()
            traj_wpts = [
                flr_rtos_api_v2.Waypoint(
                    robot_pose=flr_rtos_api_v2.RobotPose(
                        pose=state.robot_state.tool_pose,
                        joint_seeds=state.robot_state.joint_position_commands,
                    ),
                    joint_positions=state.robot_state.joint_position_commands,
                )
            ]

            if wpt:
                traj_wpts.append(wpt)
            elif wpts:
                traj_wpts.extend(wpts)
            else:
                raise Exception("Must provide at least one waypoint")

            return self.play_trajectory(traj_wpts)

        def play_trajectory(self, waypoints):
            uuid = self.load_trajectory(waypoints)
            self.play()
            return uuid

        def select_trajectory(self, uuid):
            st = flr_rtos_api_v2.SelectTrajectoryCmd()
            st.trajectory_uuid = uuid

            r = self.session.post(
                self.base_url + "/driver/trajectory/select",
                json_format.MessageToJson(st),
            )

            if r.status_code != 200:
                raise Exception("Request failed: " + json.dumps(r.json()))

        def load_trajectory(self, waypoints):
            current_state = self.get_state()
            tg = flr_rtos_api_v2.TrajectoryGenerationCmd()
            tg.inputs.waypoints.MergeFrom(waypoints)
            tg.inputs.robot.brand = self.brand
            tg.inputs.robot.model = self.model
            tg.inputs.robot.flange_to_tcp.CopyFrom(
                current_state.robot_state.tool.flange_to_tcp
            )
            tg.inputs.options.desired_uuid = str(uuid.uuid4())
            tg.inputs.robot.flange_to_tcp.CopyFrom(
                self.get_state().robot_state.tool.flange_to_tcp
            )

            r = self.session.post(
                self.base_url + "/driver/trajectory/generator",
                json_format.MessageToJson(tg),
            )

            if r.status_code != 200:
                raise Exception("Request failed: " + json.dumps(r.json()))

            return tg.inputs.options.desired_uuid

        def play(self):
            tp = flr_rtos_api_v2.TrajectoryPlayerCmd()
            tp.action = flr_rtos_api_v2.TRAJECTORY_PLAYER_ACTION_PLAY

            handle_response(
                self.session.post(
                    self.base_url + "/driver/trajectory/player",
                    json_format.MessageToJson(tp),
                )
            )

            while (
                self.get_trajectory_state().player_state
                != flr_rtos_api_v2.PLAYER_STATE_FINISHED
            ):
                time.sleep(1)

        def set_trajectory_speed_override(self, value: float):
            tos_cmd = flr_rtos_api_v2.TrajectoryOverrideSpeedCmd()
            tos_cmd.percent = value
            handle_response(
                self.session.post(
                    self.base_url + "/driver/trajectory/speed_override",
                    json_format.MessageToJson(tos_cmd),
                )
            )

        # IOs

        def get_available_ios(self):
            available_ios = flr_rtos_api_v2.AvailableIosResponse()
            json_format.Parse(
                handle_response(self.session.get(self.base_url + "/ios")).text,
                available_ios,
                ignore_unknown_fields=False,
                descriptor_pool=None,
                max_recursion_depth=100,
            )
            return available_ios

        def read_io(self, io_name: str, io_index: int = 0):
            io = flr_rtos_api_v2.ReadIoResponse()
            json_format.Parse(
                handle_response(
                    self.session.get(
                        self.base_url
                        + "/ios/"
                        + urllib.parse.quote(io_name, safe="")
                        + "?index="
                        + str(io_index)
                    )
                ).text,
                io,
                ignore_unknown_fields=False,
                descriptor_pool=None,
                max_recursion_depth=100,
            )
            return io

        def write_io(self, io_name: str, io_value: int, io_index: int = 0):
            handle_response(
                self.session.post(
                    self.base_url
                    + "/ios/"
                    + urllib.parse.quote(io_name, safe="")
                    + "?index="
                    + str(io_index)
                    + "&value="
                    + str(io_value)
                )
            )
