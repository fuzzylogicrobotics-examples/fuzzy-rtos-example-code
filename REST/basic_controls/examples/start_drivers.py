'''
MIT License

Copyright (c) 2024 Fuzzy Logic Robotics

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
'''

'''
Start driver for a specified robot and controller model
'''

import sys, os

sys.path.append(os.path.dirname(sys.path[0]))
import fuzzy_rtos_runtime_api

if __name__ == "__main__":
    frr_api = fuzzy_rtos_runtime_api.FuzzyRtosRuntimeApi("localhost")

    driver_state = frr_api.get_driver_conf()
    if driver_state.controller_id or driver_state.robot_id:
        frr_api.release_robot()

    frr_api.configure_robot(
        brand="Kuka",
        robot_model="KR 10 R1100 sixx",
        controller_model="KRC4",
        dummy_driver=True,
        remote_address="1.2.3.4",
        remote_port=42,
    )
