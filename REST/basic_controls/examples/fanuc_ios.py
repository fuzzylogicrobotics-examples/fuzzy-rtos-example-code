'''
MIT License

Copyright (c) 2024 Fuzzy Logic Robotics

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
'''

'''
Write robot IOs on a specified model of Fanuc robot
'''

import copy, os, sys
from time import sleep

sys.path.append(os.path.dirname(sys.path[0]))
from fuzzy_rtos_runtime_api import are_joints_equal, FuzzyRtosRuntimeApi, Wpt

if __name__ == "__main__":
    frr_api = FuzzyRtosRuntimeApi("localhost")

    frr_api.release_robot()

    # Connect to a virtual robot in Roboguide
    robot = frr_api.configure_robot(
        brand="Fanuc",
        robot_model="LR Mate 200iD/7L",
        controller_model="R-30iB-Plus",
        dummy_driver=False,
        local_address="127.0.0.1",
        remote_address="127.0.0.1",
    )

    print(robot.get_available_ios().ios)

    robot.write_io("RO", io_index=3, io_value=1)
    print(robot.read_io("RO", 3).io)

    sleep(2)

    robot.write_io("RO", io_index=3, io_value=0)
    print(robot.read_io("RO", 3).io)
