'''
MIT License

Copyright (c) 2024 Fuzzy Logic Robotics

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
'''

'''
Connect to Fuzzy RTOS
'''

import sys, os

sys.path.append(os.path.dirname(sys.path[0]))
import fuzzy_rtos_runtime_api

if __name__ == "__main__":
    frr_api = fuzzy_rtos_runtime_api.FuzzyRtosRuntimeApi("localhost")
    print("Connected to Fuzzy RTOS version " + frr_api.get_version())

    print("")
    print("License information:")
    if frr_api.get_license_validity():
        print("  Current license: " + frr_api.get_license_name())
        # print("  License key: " + frr_api.get_license_key())  # license key request is not available in Fuzzy RTOS Runtime Sim which does not have a license
        print("  Validity: " , frr_api.get_license_validity())

    print("")
    print("Available robots:")
    robots = frr_api.get_all_robots()
    for r in robots:
        print("  " + r.brand + " " + r.model + " (" + str(r.id) + ")")

    print("")
    print("Available controllers:")
    controllers = frr_api.get_all_controllers()
    for c in controllers:
        print("  " + c.brand + " " + c.model + " (" + str(c.id) + ")")

    print()
    try:
        driver_state = frr_api.get_driver_conf()
        current_c = frr_api.get_controller_by_id(driver_state.controller_id)
        current_r = frr_api.get_robot_by_id(driver_state.robot_id)
        print("Current setup:" + (" (dummy)" if driver_state.is_dummy_driver else ""))
        print("  Controller: " + current_c.brand + " " + current_c.model)
        print("  Robot:      " + current_r.brand + " " + current_r.model)
    except:
        print("Drivers are not loaded")

    exit(0)
