'''
MIT License

Copyright (c) 2024 Fuzzy Logic Robotics

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
'''

'''
Move the robot
'''

import copy, os, sys

sys.path.append(os.path.dirname(sys.path[0]))
from fuzzy_rtos_runtime_api import are_joints_equal, FuzzyRtosRuntimeApi, Wpt

if __name__ == "__main__":
    frr_api = FuzzyRtosRuntimeApi("localhost")
    robot = frr_api.get_robot()

    robot_state = robot.get_state()

    home_q = robot.get_home_position()

    if not are_joints_equal(robot_state.robot_state.joint_positions, home_q):
        robot.move(Wpt.Joint(home_q))

    target_a = robot.fk(home_q) # forward kinematics
    target_a.translation.x += 0.1

    target_b = copy.copy(target_a)
    target_b.translation.y += 0.1

    target_c = copy.copy(target_b)
    target_c.translation.x -= 0.1

    target_d = robot.fk(home_q)
    target_d.translation.z -= 0.3

    robot.move(
        wpts=[
            Wpt.Linear(target_a, b=100),
            Wpt.Linear(target_b),
            Wpt.Spline(target_c),
            Wpt.Spline(target_d),
        ]
    )
