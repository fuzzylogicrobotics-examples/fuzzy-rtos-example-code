# Jog any two robots at once

Connect with the same PC to two Fuzzy RTOS IPCs connected to two different robots and jog them simultaneously with a joystick using the REST interface.

The two IPCs must have a distinct IP address.

You can jog any two robots of any brand supported by Fuzzy RTOS.

![Jog two robots at once](https://gitlab.com/fuzzylogicrobotics-examples/fuzzy-rtos-example-code/-/raw/main/REST/jogg_two_robots_at_once/jog_two_robots.gif?ref_type=heads)