'''
MIT License

Copyright (c) 2024 Fuzzy Logic Robotics

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
'''

'''
Jog two robots from any brand simultaneously with a joystick by connection to two IPCs at the same time
'''

import pygame
import copy, os, sys

sys.path.append(os.path.dirname(sys.path[0]))
sys.path.append('protos')
import fuzzy_rtos_runtime_api
from protos import api_v2_pb2 as flr_api_v2


def main():
    frr_api = fuzzy_rtos_runtime_api.FuzzyRtosRuntimeApi("RTOS1_IP")
    robot1 = frr_api.get_robot()
    frr_api2 = fuzzy_rtos_runtime_api.FuzzyRtosRuntimeApi("RTOS2_IP")
    robot2 = frr_api2.get_robot()

    clock = pygame.time.Clock()

    joysticks = {}

    x_joystick_id = 1  # left joystick up (-1) / down (1)
    y_joystick_id = 0  # left joystick left (-1) / right (1)
    z_joystick_id = 3  # right joystick up (-1) / down (1)

    deadman_button_id = 4  # LB
    alt_mode_button_id = 5  # RB
    quit_button_id = 7  # Burger

    joystick_instance_id = None
    alt_mode = False

    done = False
    while not done:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                done = True

            if event.type == pygame.JOYBUTTONDOWN:
                print(f"Joystick button {event.button} pressed.")
                if event.button == deadman_button_id:
                    joystick_instance_id = copy.copy(event.instance_id)
                    robot1.set_control_mode_jogging()
                    robot2.set_control_mode_jogging()
                if event.button == alt_mode_button_id:
                    alt_mode = True
                if event.button == quit_button_id:
                    done = True

            if event.type == pygame.JOYBUTTONUP:
                print(f"Joystick button {event.button} released.")
                if event.button == deadman_button_id:
                    joystick_instance_id = None
                    try:
                        robot1.set_control_mode_idle()
                        robot2.set_control_mode_idle()
                    except:
                        pass
                if event.button == alt_mode_button_id:
                    alt_mode = False

            if event.type == pygame.JOYDEVICEADDED:
                joy = pygame.joystick.Joystick(event.device_index)
                joysticks[joy.get_instance_id()] = joy
                print(f"Joystick {joy.get_instance_id()} connected")

            if event.type == pygame.JOYDEVICEREMOVED:
                del joysticks[event.instance_id]
                print(f"Joystick {event.instance_id} disconnected")

        if joystick_instance_id != None:
            joystick = joysticks[joystick_instance_id]
            twist = flr_api_v2.Twist()
            if alt_mode:
                twist.angular.x = -0.1 * joystick.get_axis(x_joystick_id)
                twist.angular.y = -0.1 * joystick.get_axis(y_joystick_id)
                twist.angular.z = -0.1 * joystick.get_axis(z_joystick_id)
            else:
                twist.linear.x = -0.1 * joystick.get_axis(x_joystick_id)
                twist.linear.y = -0.1 * joystick.get_axis(y_joystick_id)
                twist.linear.z = -0.1 * joystick.get_axis(z_joystick_id)
            try:
                robot1.cartesian_velocity_jog(xd=twist)
                robot2.cartesian_velocity_jog(xd=twist)
            except:
                pass

        clock.tick(100)


if __name__ == "__main__":
    pygame.init()
    main()
    pygame.quit()
