'''
MIT License

Copyright (c) 2024 Fuzzy Logic Robotics

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
'''

'''
Receive and print the value of a bool IO from Fuzzy Studio, then set the Robot IO value depending on the bool value.
Import the provided device or create your own. Don't forget the change the mqtt topic name to match the name of you device.
To read the value, you must send a value from studio by associating the IO to a trajectory WP, forcing the value, or sending a test directly
from the exchange table.

'''

import requests
import paho.mqtt.client as mqtt


base_url = 'http://192.168.10.254:8000' # 192.168.10.254 for typical Fuzzy RTOS fixed IP adress
io_name = "ABB_Scalable_IO_0_DO1" # change the value of this variable to the name of your IO

# setup io communication


def make_request(endpoint, method, data=None):
    url = base_url + endpoint
    method = method.upper()

    if method == 'GET':
        response = requests.get(url)
    elif method == 'POST':
        response = requests.post(url, json=data)
    elif method == 'PUT':
        response = requests.put(url, json=data)
    elif method == 'DELETE':
        response = requests.delete(url)
    else:
        raise ValueError(f"Unsupported HTTP method: {method}")
    
    print("status code", response.status_code)
    successful_request = response.status_code == 200 or response.status_code == 201 or response.status_code == 204

    if successful_request:
        print(method + 'Request Successful')
        if method == 'GET':
            print('Response:', response.json())
    else:
        print(method + 'Request Failed')
        print('Status Code:', response.status_code)
        print('Response:', response.text)

    return response


# setup mqtt communication

mqtt_broker = 'localhost'
mqtt_port = 1883    # port used by Fuzzy Studio
mqtt_topic = '/fuzzy_studio/io/plc/to_device'   # topic name on which Fuzzy Studio publihes, can be found on the device window in the exchange tables

def on_connect(client, userdata, flags, reason_code, properties):
    print("Connected with result code " + str(reason_code))
    # Subscribe to the topic
    client.subscribe(mqtt_topic)

# when message is received
def on_message(client, userdata, message):

    # if the io is a boolean
    boolean_value = bool(message.payload[0])
    print(f"Received boolean: {boolean_value}")

    # set IO to a certain value

    if boolean_value == True:
        data = {
            "ios": [
                {
                    "name": io_name,
                    "value": "1",   # change the value for the value you want to set your IO to if the received message is True
                    "index": 0
                }
            ]
        }

    else:
        data = {
            "ios": [
                {
                    "name": io_name,
                    "value": "0",   # change the value for the value you want to set your IO to if the received message is False
                    "index": 0
                }
            ]
        }


    make_request('/ios/write', 'POST', data)


def on_subscribe(client, userdata, mid, rc, granted_qos):
    print("subscribed successfully")

# Create an MQTT client instance
client = mqtt.Client(mqtt.CallbackAPIVersion.VERSION2)

# Assign the callback functions
client.on_connect = on_connect
client.on_message = on_message
client.on_subscribe = on_subscribe

# Connect to the MQTT broker
client.connect(mqtt_broker, mqtt_port)

# Start the loop to process callbacks and handle reconnections and messages
client.loop_forever()


if __name__ == "__main__":

    # Connect to the MQTT broker
    client.connect(mqtt_broker, mqtt_port)

    # Start the loop to process callbacks and handle reconnections and messages
    client.loop_forever()

    