# Set an IO on the robot controller with the REST interface

Refer to [this section of the documentation](https://fuzzylogicrobotics.gitlab.io/fuzzy_docs/docs/4.10.0/fuzzy-rtos/usage/read-write-ios) for more informaton on reading and triggering robots IOs.

Note that this feature is available from version 4.10 onward on Fanuc and ABB robots only and is currently in beta mode. It is subject to changes between the next version to stabilize the API.


