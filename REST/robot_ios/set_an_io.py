'''
MIT License

Copyright (c) 2024 Fuzzy Logic Robotics

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
'''

'''

Set a robot IO

'''

import requests

base_url = 'http://192.168.10.254:8000' # 192.168.10.254 for typical Fuzzy RTOS fixed IP adress
io_name = "ABB_Scalable_IO_0_DO1" # change the value of this variable to the name of your IO


def make_request(endpoint, method, data=None):
    url = base_url + endpoint
    method = method.upper()

    if method == 'GET':
        response = requests.get(url)
    elif method == 'POST':
        response = requests.post(url, json=data)
    elif method == 'PUT':
        response = requests.put(url, json=data)
    elif method == 'DELETE':
        response = requests.delete(url)
    else:
        raise ValueError(f"Unsupported HTTP method: {method}")
    
    print("status code", response.status_code)
    successful_request = response.status_code == 200 or response.status_code == 201 or response.status_code == 204

    if successful_request:
        print(method + 'Request Successful')
        if method == 'GET':
            print('Response:', response.json())
    else:
        print(method + 'Request Failed')
        print('Status Code:', response.status_code)
        print('Response:', response.text)

    return response

if __name__ == "__main__":

    # make_request('/ios', 'GET')

    # set an IO to a certain value
    data = {
        "ios": [
            {
                "name": io_name,
                "value": "1",
                "index": 0
            }
        ]
    }

    make_request('/ios/write', 'POST', data)