# Fuzzy RTOS API and Fuzzy Studio IOs example codes

**In this repository, you will find example codes for the Fuzzy RTOS API through the MQTT, UDP and REST interfaces as well as sample codes demonstrating the use of Fuzzy Studio IOs.**

## ⚠️ WARNING ⚠️

This code is provided as an example only and is **not intended** for production use. Please use it as a learning tool. Fuzzy Logic won't be responsible for its use in live projects. 

Also please note that the REST API is still under development, with changes and enhancements expected in future software versions.

Enjoy exploring!

## What is the Fuzzy RTOS API

Fuzzy RTOS uses MQTT (Message Queuing Telemetry Transport) and REST technologies to convert user commands into realtime low-level robot control. It works seamlessly across all major robot brands, allowing you to quickly create robotic applications using modern languages, like C#, python, javascript, and connect them to any industrial equipment. 

**[Please visit this page to access the online documentation for the Fuzzy RTOS API.](https://fuzzylogicrobotics.gitlab.io/fuzzy_docs/docs/4.10.0/fuzzy-rtos/usage/interfaces/)**

## What are Fuzzy Studio IOs

When using Fuzzy Studio and when you are connected to a real robot you can use I/Os in order to communicate with exterior tools.
Our I/Os are working with the MQTT Protocol and through exchange tables.

**[Please visit this page to access the online documentation for the Fuzzy Studio IO Management](https://fuzzylogicrobotics.gitlab.io/fuzzy_docs/docs/4.11.0/Fuzzy%20Studio/Trajectories%20panel/IO%20Management/)**


## For all you simulated tests: Simulate a Fuzzy RTOS to try these codes in full simulation

To simulate a connection to a real Fuzzy RTOS and be able to test these scripts without the need to connect to a real robot, follow the steps illustrated in the following GIF:

- Launch an instance of Fuzzy RTOS runtime sim, automatically installed on your computer with Fuzzy Studio.
- In Fuzzy Studio, switch to **Physical** then open the Fuzzy RTOS wizard, just as you would to connect to a real robot.
- Select **Add manually** and enter **localhost**
- Choose and connect to the local Fuzzy RTOS

![Local Fuzzy RTOS connection](https://gitlab.com/fuzzylogicrobotics-examples/fuzzy-rtos-example-code/-/raw/dev/local_fuzzy_rtos_connection.gif)

You can now launch any script that requires a physical connection to a robot.

