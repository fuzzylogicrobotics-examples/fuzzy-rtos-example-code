'''
MIT License

Copyright (c) 2024 Fuzzy Logic Robotics

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
'''

'''

Print the current system state using UDP

'''

import socket
import time
import sys
import os
sys.path.append('protos')
from protos import api_v2_rtos_pb2 as flr_rtos_api_v2
from protos import api_v2_pb2 as flr_api_v2 
from google.protobuf.any_pb2 import Any

# UDP server configuration
UDP_IP = "localhost" # RTOS IP
UPD_PORT = 51218     # flr encoded in alphabetical number but instead of 6 pour 'f' it's 5
RTOS_ADRESS = (UDP_IP, UPD_PORT)

# create UDP socket for receiving
udp_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

# create UDP socket for sending
udp_socket_send = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

def send(message):
    msg = Any()                                 # creates protobug
    msg.Pack(message)                               # fills protobuf
    serial_msg = msg.SerializeToString()        # serialize the message to bytes
    udp_sock.sendto(serial_msg, RTOS_ADRESS)
    print(f"Sent UDP message: {message}")


def receive( response_message_type):
    response_data, server = udp_sock.recvfrom(1024) # receive data from the socket
    response_message = response_message_type()      # initialise the response message of the specified type
    response_message.ParseFromString(response_data) # parse received data into the response message
    print("Received response:", response_message)
    return response_message


def main():
    try:
        # send any message to be able to receive
        rtos_ctrl_mode_cmd = flr_rtos_api_v2.RTOSControlModeCmd(control_mode_request=flr_rtos_api_v2.RTOS_CONTROL_MODE_JOGGING)
        send(rtos_ctrl_mode_cmd)

        # get messages from RTOS
        receive(flr_rtos_api_v2.SystemState)    # get system state
        

    finally:
        udp_sock.close()


if __name__=="__main__":
   main()
