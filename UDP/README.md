# UDP example codes

For a more direct link between Fuzzy RTOS and custom applications, we provide an UDP socket interface. By default, it listens for incoming protobuf messages formatted as a [Any message](https://github.com/protocolbuffers/protobuf/blob/main/src/google/protobuf/any.proto) (see the link for documentation and examples) **on port 51218.**

If Fuzzy RTOS is controlling a robot, it will stream a SystemState protobuf message to the sender of the last received message.

We rely on protobuf to format our messages, whether in binary form or serialized as JSON.
The description files are available here: [api_v2.proto](https://fuzzylogicrobotics.gitlab.io/fuzzy_docs/assets/files/api_v2-fa117a011a562d053b816d562b252c95.proto) and [api_v2_rtos.proto.](https://fuzzylogicrobotics.gitlab.io/fuzzy_docs/assets/files/api_v2_rtos-36796f19ebf0a53a60605c3358d54b12.proto) api_v2_rtos.proto.

