from protos import api_v2_pb2 as _api_v2_pb2
from google.protobuf.internal import containers as _containers
from google.protobuf.internal import enum_type_wrapper as _enum_type_wrapper
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from typing import ClassVar as _ClassVar, Iterable as _Iterable, Mapping as _Mapping, Optional as _Optional, Union as _Union

DESCRIPTOR: _descriptor.FileDescriptor

class RTOSControlMode(int, metaclass=_enum_type_wrapper.EnumTypeWrapper):
    __slots__ = []
    RTOS_CONTROL_MODE_UNSPECIFIED: _ClassVar[RTOSControlMode]
    RTOS_CONTROL_MODE_IDLE: _ClassVar[RTOSControlMode]
    RTOS_CONTROL_MODE_JOGGING: _ClassVar[RTOSControlMode]
    RTOS_CONTROL_MODE_TRAJECTORY: _ClassVar[RTOSControlMode]

class OperationalMode(int, metaclass=_enum_type_wrapper.EnumTypeWrapper):
    __slots__ = []
    OPERATIONAL_MODE_UNSPECIFIED: _ClassVar[OperationalMode]
    OPERATIONAL_MODE_MANUAL: _ClassVar[OperationalMode]
    OPERATIONAL_MODE_AUTO: _ClassVar[OperationalMode]

class SpeedMode(int, metaclass=_enum_type_wrapper.EnumTypeWrapper):
    __slots__ = []
    SPEED_MODE_UNSPECIFIED: _ClassVar[SpeedMode]
    SPEED_MODE_SLOW: _ClassVar[SpeedMode]
    SPEED_MODE_FAST: _ClassVar[SpeedMode]
    SPEED_MODE_MAX: _ClassVar[SpeedMode]

class ConnectionState(int, metaclass=_enum_type_wrapper.EnumTypeWrapper):
    __slots__ = []
    CONNECTION_STATE_UNSPECIFIED: _ClassVar[ConnectionState]
    CONNECTION_STATE_CONNECTED: _ClassVar[ConnectionState]
    CONNECTION_STATE_NOT_CONNECTED: _ClassVar[ConnectionState]
    CONNECTION_STATE_UNREACHABLE: _ClassVar[ConnectionState]

class TrajectoryPlayerAction(int, metaclass=_enum_type_wrapper.EnumTypeWrapper):
    __slots__ = []
    TRAJECTORY_PLAYER_ACTION_UNSPECIFIED: _ClassVar[TrajectoryPlayerAction]
    TRAJECTORY_PLAYER_ACTION_PAUSE: _ClassVar[TrajectoryPlayerAction]
    TRAJECTORY_PLAYER_ACTION_PLAY: _ClassVar[TrajectoryPlayerAction]
    TRAJECTORY_PLAYER_ACTION_REWIND: _ClassVar[TrajectoryPlayerAction]

class PlayerState(int, metaclass=_enum_type_wrapper.EnumTypeWrapper):
    __slots__ = []
    PLAYER_STATE_UNSPECIFIED: _ClassVar[PlayerState]
    PLAYER_STATE_STOPPED: _ClassVar[PlayerState]
    PLAYER_STATE_PAUSED: _ClassVar[PlayerState]
    PLAYER_STATE_PLAYING: _ClassVar[PlayerState]
    PLAYER_STATE_REWINDING: _ClassVar[PlayerState]
    PLAYER_STATE_CONVERGING_TOWARDS_TARGET: _ClassVar[PlayerState]
    PLAYER_STATE_FINISHED: _ClassVar[PlayerState]

class MotionType(int, metaclass=_enum_type_wrapper.EnumTypeWrapper):
    __slots__ = []
    MOTION_TYPE_UNSPECIFIED: _ClassVar[MotionType]
    MOTION_TYPE_PTP: _ClassVar[MotionType]
    MOTION_TYPE_LIN: _ClassVar[MotionType]
    MOTION_TYPE_SPLINE: _ClassVar[MotionType]
    MOTION_TYPE_JOINT_SPLINE: _ClassVar[MotionType]

class BlendingSpeedType(int, metaclass=_enum_type_wrapper.EnumTypeWrapper):
    __slots__ = []
    BLENDING_SPEED_TYPE_UNSPECIFIED: _ClassVar[BlendingSpeedType]
    BLENDING_SPEED_TYPE_AUTO: _ClassVar[BlendingSpeedType]
    BLENDING_SPEED_TYPE_VARYING: _ClassVar[BlendingSpeedType]
    BLENDING_SPEED_TYPE_CONSTANT: _ClassVar[BlendingSpeedType]

class GeneratorState(int, metaclass=_enum_type_wrapper.EnumTypeWrapper):
    __slots__ = []
    GENERATOR_STATE_UNSPECIFIED: _ClassVar[GeneratorState]
    GENERATOR_STATE_IDLE: _ClassVar[GeneratorState]
    GENERATOR_STATE_BUSY: _ClassVar[GeneratorState]

class GenerationResult(int, metaclass=_enum_type_wrapper.EnumTypeWrapper):
    __slots__ = []
    GENERATOR_RESULT_UNSPECIFIED: _ClassVar[GenerationResult]
    GENERATOR_RESULT_INPROGRESS: _ClassVar[GenerationResult]
    GENERATOR_RESULT_SUCCESS: _ClassVar[GenerationResult]
    GENERATOR_RESULT_FAILURE: _ClassVar[GenerationResult]

class IoType(int, metaclass=_enum_type_wrapper.EnumTypeWrapper):
    __slots__ = []
    IOTYPE_UNSPECIFIED: _ClassVar[IoType]
    IOTYPE_INPUT: _ClassVar[IoType]
    IOTYPE_OUTPUT: _ClassVar[IoType]

class IoValueType(int, metaclass=_enum_type_wrapper.EnumTypeWrapper):
    __slots__ = []
    IODATATYPE_UNSPECIFIED: _ClassVar[IoValueType]
    IODATATYPE_BOOL: _ClassVar[IoValueType]
    IODATATYPE_UINT8: _ClassVar[IoValueType]
    IODATATYPE_INT8: _ClassVar[IoValueType]
    IODATATYPE_UINT16: _ClassVar[IoValueType]
    IODATATYPE_INT16: _ClassVar[IoValueType]
RTOS_CONTROL_MODE_UNSPECIFIED: RTOSControlMode
RTOS_CONTROL_MODE_IDLE: RTOSControlMode
RTOS_CONTROL_MODE_JOGGING: RTOSControlMode
RTOS_CONTROL_MODE_TRAJECTORY: RTOSControlMode
OPERATIONAL_MODE_UNSPECIFIED: OperationalMode
OPERATIONAL_MODE_MANUAL: OperationalMode
OPERATIONAL_MODE_AUTO: OperationalMode
SPEED_MODE_UNSPECIFIED: SpeedMode
SPEED_MODE_SLOW: SpeedMode
SPEED_MODE_FAST: SpeedMode
SPEED_MODE_MAX: SpeedMode
CONNECTION_STATE_UNSPECIFIED: ConnectionState
CONNECTION_STATE_CONNECTED: ConnectionState
CONNECTION_STATE_NOT_CONNECTED: ConnectionState
CONNECTION_STATE_UNREACHABLE: ConnectionState
TRAJECTORY_PLAYER_ACTION_UNSPECIFIED: TrajectoryPlayerAction
TRAJECTORY_PLAYER_ACTION_PAUSE: TrajectoryPlayerAction
TRAJECTORY_PLAYER_ACTION_PLAY: TrajectoryPlayerAction
TRAJECTORY_PLAYER_ACTION_REWIND: TrajectoryPlayerAction
PLAYER_STATE_UNSPECIFIED: PlayerState
PLAYER_STATE_STOPPED: PlayerState
PLAYER_STATE_PAUSED: PlayerState
PLAYER_STATE_PLAYING: PlayerState
PLAYER_STATE_REWINDING: PlayerState
PLAYER_STATE_CONVERGING_TOWARDS_TARGET: PlayerState
PLAYER_STATE_FINISHED: PlayerState
MOTION_TYPE_UNSPECIFIED: MotionType
MOTION_TYPE_PTP: MotionType
MOTION_TYPE_LIN: MotionType
MOTION_TYPE_SPLINE: MotionType
MOTION_TYPE_JOINT_SPLINE: MotionType
BLENDING_SPEED_TYPE_UNSPECIFIED: BlendingSpeedType
BLENDING_SPEED_TYPE_AUTO: BlendingSpeedType
BLENDING_SPEED_TYPE_VARYING: BlendingSpeedType
BLENDING_SPEED_TYPE_CONSTANT: BlendingSpeedType
GENERATOR_STATE_UNSPECIFIED: GeneratorState
GENERATOR_STATE_IDLE: GeneratorState
GENERATOR_STATE_BUSY: GeneratorState
GENERATOR_RESULT_UNSPECIFIED: GenerationResult
GENERATOR_RESULT_INPROGRESS: GenerationResult
GENERATOR_RESULT_SUCCESS: GenerationResult
GENERATOR_RESULT_FAILURE: GenerationResult
IOTYPE_UNSPECIFIED: IoType
IOTYPE_INPUT: IoType
IOTYPE_OUTPUT: IoType
IODATATYPE_UNSPECIFIED: IoValueType
IODATATYPE_BOOL: IoValueType
IODATATYPE_UINT8: IoValueType
IODATATYPE_INT8: IoValueType
IODATATYPE_UINT16: IoValueType
IODATATYPE_INT16: IoValueType

class RunOnRealRobotCmd(_message.Message):
    __slots__ = ["header", "run_on_real_robot"]
    HEADER_FIELD_NUMBER: _ClassVar[int]
    RUN_ON_REAL_ROBOT_FIELD_NUMBER: _ClassVar[int]
    header: _api_v2_pb2.CmdHeader
    run_on_real_robot: bool
    def __init__(self, header: _Optional[_Union[_api_v2_pb2.CmdHeader, _Mapping]] = ..., run_on_real_robot: bool = ...) -> None: ...

class RTOSControlModeCmd(_message.Message):
    __slots__ = ["header", "control_mode_request"]
    HEADER_FIELD_NUMBER: _ClassVar[int]
    CONTROL_MODE_REQUEST_FIELD_NUMBER: _ClassVar[int]
    header: _api_v2_pb2.CmdHeader
    control_mode_request: RTOSControlMode
    def __init__(self, header: _Optional[_Union[_api_v2_pb2.CmdHeader, _Mapping]] = ..., control_mode_request: _Optional[_Union[RTOSControlMode, str]] = ...) -> None: ...

class Tool(_message.Message):
    __slots__ = ["name", "flange_to_tcp"]
    NAME_FIELD_NUMBER: _ClassVar[int]
    FLANGE_TO_TCP_FIELD_NUMBER: _ClassVar[int]
    name: str
    flange_to_tcp: _api_v2_pb2.Pose
    def __init__(self, name: _Optional[str] = ..., flange_to_tcp: _Optional[_Union[_api_v2_pb2.Pose, _Mapping]] = ...) -> None: ...

class ToolCmd(_message.Message):
    __slots__ = ["header", "tool"]
    HEADER_FIELD_NUMBER: _ClassVar[int]
    TOOL_FIELD_NUMBER: _ClassVar[int]
    header: _api_v2_pb2.CmdHeader
    tool: Tool
    def __init__(self, header: _Optional[_Union[_api_v2_pb2.CmdHeader, _Mapping]] = ..., tool: _Optional[_Union[Tool, _Mapping]] = ...) -> None: ...

class JoggingSpeedOverrideCmd(_message.Message):
    __slots__ = ["header", "percent"]
    HEADER_FIELD_NUMBER: _ClassVar[int]
    PERCENT_FIELD_NUMBER: _ClassVar[int]
    header: _api_v2_pb2.CmdHeader
    percent: float
    def __init__(self, header: _Optional[_Union[_api_v2_pb2.CmdHeader, _Mapping]] = ..., percent: _Optional[float] = ...) -> None: ...

class CartesianPoseJoggingCmd(_message.Message):
    __slots__ = ["header", "desired_pose"]
    HEADER_FIELD_NUMBER: _ClassVar[int]
    DESIRED_POSE_FIELD_NUMBER: _ClassVar[int]
    header: _api_v2_pb2.CmdHeader
    desired_pose: _api_v2_pb2.Pose
    def __init__(self, header: _Optional[_Union[_api_v2_pb2.CmdHeader, _Mapping]] = ..., desired_pose: _Optional[_Union[_api_v2_pb2.Pose, _Mapping]] = ...) -> None: ...

class CartesianVelocityJoggingCmd(_message.Message):
    __slots__ = ["header", "desired_velocity"]
    HEADER_FIELD_NUMBER: _ClassVar[int]
    DESIRED_VELOCITY_FIELD_NUMBER: _ClassVar[int]
    header: _api_v2_pb2.CmdHeader
    desired_velocity: _api_v2_pb2.Twist
    def __init__(self, header: _Optional[_Union[_api_v2_pb2.CmdHeader, _Mapping]] = ..., desired_velocity: _Optional[_Union[_api_v2_pb2.Twist, _Mapping]] = ...) -> None: ...

class JointPositionJoggingCmd(_message.Message):
    __slots__ = ["header", "joint_positions"]
    HEADER_FIELD_NUMBER: _ClassVar[int]
    JOINT_POSITIONS_FIELD_NUMBER: _ClassVar[int]
    header: _api_v2_pb2.CmdHeader
    joint_positions: _api_v2_pb2.Joints
    def __init__(self, header: _Optional[_Union[_api_v2_pb2.CmdHeader, _Mapping]] = ..., joint_positions: _Optional[_Union[_api_v2_pb2.Joints, _Mapping]] = ...) -> None: ...

class JointVelocityJoggingCmd(_message.Message):
    __slots__ = ["header", "joint_velocities"]
    HEADER_FIELD_NUMBER: _ClassVar[int]
    JOINT_VELOCITIES_FIELD_NUMBER: _ClassVar[int]
    header: _api_v2_pb2.CmdHeader
    joint_velocities: _api_v2_pb2.Joints
    def __init__(self, header: _Optional[_Union[_api_v2_pb2.CmdHeader, _Mapping]] = ..., joint_velocities: _Optional[_Union[_api_v2_pb2.Joints, _Mapping]] = ...) -> None: ...

class OperationalSwitchCmd(_message.Message):
    __slots__ = ["header", "operational_mode", "speed_mode"]
    HEADER_FIELD_NUMBER: _ClassVar[int]
    OPERATIONAL_MODE_FIELD_NUMBER: _ClassVar[int]
    SPEED_MODE_FIELD_NUMBER: _ClassVar[int]
    header: _api_v2_pb2.CmdHeader
    operational_mode: OperationalMode
    speed_mode: SpeedMode
    def __init__(self, header: _Optional[_Union[_api_v2_pb2.CmdHeader, _Mapping]] = ..., operational_mode: _Optional[_Union[OperationalMode, str]] = ..., speed_mode: _Optional[_Union[SpeedMode, str]] = ...) -> None: ...

class DeadmanHeartbeatCmd(_message.Message):
    __slots__ = ["header"]
    HEADER_FIELD_NUMBER: _ClassVar[int]
    header: _api_v2_pb2.CmdHeader
    def __init__(self, header: _Optional[_Union[_api_v2_pb2.CmdHeader, _Mapping]] = ...) -> None: ...

class RobotControllerState(_message.Message):
    __slots__ = ["header", "name", "ready_to_execute_commands", "safety_system_ready", "robot_script_running", "power_stage_enabled", "number_of_late_packets", "latest_robot_language_setpoint"]
    HEADER_FIELD_NUMBER: _ClassVar[int]
    NAME_FIELD_NUMBER: _ClassVar[int]
    READY_TO_EXECUTE_COMMANDS_FIELD_NUMBER: _ClassVar[int]
    SAFETY_SYSTEM_READY_FIELD_NUMBER: _ClassVar[int]
    ROBOT_SCRIPT_RUNNING_FIELD_NUMBER: _ClassVar[int]
    POWER_STAGE_ENABLED_FIELD_NUMBER: _ClassVar[int]
    NUMBER_OF_LATE_PACKETS_FIELD_NUMBER: _ClassVar[int]
    LATEST_ROBOT_LANGUAGE_SETPOINT_FIELD_NUMBER: _ClassVar[int]
    header: _api_v2_pb2.StateHeader
    name: str
    ready_to_execute_commands: bool
    safety_system_ready: bool
    robot_script_running: bool
    power_stage_enabled: bool
    number_of_late_packets: int
    latest_robot_language_setpoint: _api_v2_pb2.Joints
    def __init__(self, header: _Optional[_Union[_api_v2_pb2.StateHeader, _Mapping]] = ..., name: _Optional[str] = ..., ready_to_execute_commands: bool = ..., safety_system_ready: bool = ..., robot_script_running: bool = ..., power_stage_enabled: bool = ..., number_of_late_packets: _Optional[int] = ..., latest_robot_language_setpoint: _Optional[_Union[_api_v2_pb2.Joints, _Mapping]] = ...) -> None: ...

class RTOSState(_message.Message):
    __slots__ = ["header", "version", "executing_on_real_robot", "speed_mode", "operational_mode", "robot_control_mode", "connection_state", "allowed_events", "latest_position_setpoints", "joint_position_setpoints", "joint_velocity_setpoints", "tool_pose_setpoint", "tool_velocity_setpoint", "jogging_speed_override_percent"]
    HEADER_FIELD_NUMBER: _ClassVar[int]
    VERSION_FIELD_NUMBER: _ClassVar[int]
    EXECUTING_ON_REAL_ROBOT_FIELD_NUMBER: _ClassVar[int]
    SPEED_MODE_FIELD_NUMBER: _ClassVar[int]
    OPERATIONAL_MODE_FIELD_NUMBER: _ClassVar[int]
    ROBOT_CONTROL_MODE_FIELD_NUMBER: _ClassVar[int]
    CONNECTION_STATE_FIELD_NUMBER: _ClassVar[int]
    ALLOWED_EVENTS_FIELD_NUMBER: _ClassVar[int]
    LATEST_POSITION_SETPOINTS_FIELD_NUMBER: _ClassVar[int]
    JOINT_POSITION_SETPOINTS_FIELD_NUMBER: _ClassVar[int]
    JOINT_VELOCITY_SETPOINTS_FIELD_NUMBER: _ClassVar[int]
    TOOL_POSE_SETPOINT_FIELD_NUMBER: _ClassVar[int]
    TOOL_VELOCITY_SETPOINT_FIELD_NUMBER: _ClassVar[int]
    JOGGING_SPEED_OVERRIDE_PERCENT_FIELD_NUMBER: _ClassVar[int]
    header: _api_v2_pb2.StateHeader
    version: str
    executing_on_real_robot: bool
    speed_mode: SpeedMode
    operational_mode: OperationalMode
    robot_control_mode: RTOSControlMode
    connection_state: ConnectionState
    allowed_events: _containers.RepeatedScalarFieldContainer[str]
    latest_position_setpoints: _api_v2_pb2.Joints
    joint_position_setpoints: _api_v2_pb2.Joints
    joint_velocity_setpoints: _api_v2_pb2.Joints
    tool_pose_setpoint: _api_v2_pb2.Pose
    tool_velocity_setpoint: _api_v2_pb2.Twist
    jogging_speed_override_percent: float
    def __init__(self, header: _Optional[_Union[_api_v2_pb2.StateHeader, _Mapping]] = ..., version: _Optional[str] = ..., executing_on_real_robot: bool = ..., speed_mode: _Optional[_Union[SpeedMode, str]] = ..., operational_mode: _Optional[_Union[OperationalMode, str]] = ..., robot_control_mode: _Optional[_Union[RTOSControlMode, str]] = ..., connection_state: _Optional[_Union[ConnectionState, str]] = ..., allowed_events: _Optional[_Iterable[str]] = ..., latest_position_setpoints: _Optional[_Union[_api_v2_pb2.Joints, _Mapping]] = ..., joint_position_setpoints: _Optional[_Union[_api_v2_pb2.Joints, _Mapping]] = ..., joint_velocity_setpoints: _Optional[_Union[_api_v2_pb2.Joints, _Mapping]] = ..., tool_pose_setpoint: _Optional[_Union[_api_v2_pb2.Pose, _Mapping]] = ..., tool_velocity_setpoint: _Optional[_Union[_api_v2_pb2.Twist, _Mapping]] = ..., jogging_speed_override_percent: _Optional[float] = ...) -> None: ...

class RobotState(_message.Message):
    __slots__ = ["header", "joint_names", "joint_positions", "joint_velocities", "joint_accelerations", "joint_jerks", "tool", "tool_pose", "tool_velocity", "tool_acceleration", "joint_position_commands"]
    HEADER_FIELD_NUMBER: _ClassVar[int]
    JOINT_NAMES_FIELD_NUMBER: _ClassVar[int]
    JOINT_POSITIONS_FIELD_NUMBER: _ClassVar[int]
    JOINT_VELOCITIES_FIELD_NUMBER: _ClassVar[int]
    JOINT_ACCELERATIONS_FIELD_NUMBER: _ClassVar[int]
    JOINT_JERKS_FIELD_NUMBER: _ClassVar[int]
    TOOL_FIELD_NUMBER: _ClassVar[int]
    TOOL_POSE_FIELD_NUMBER: _ClassVar[int]
    TOOL_VELOCITY_FIELD_NUMBER: _ClassVar[int]
    TOOL_ACCELERATION_FIELD_NUMBER: _ClassVar[int]
    JOINT_POSITION_COMMANDS_FIELD_NUMBER: _ClassVar[int]
    header: _api_v2_pb2.StateHeader
    joint_names: _containers.RepeatedScalarFieldContainer[str]
    joint_positions: _api_v2_pb2.Joints
    joint_velocities: _api_v2_pb2.Joints
    joint_accelerations: _api_v2_pb2.Joints
    joint_jerks: _api_v2_pb2.Joints
    tool: Tool
    tool_pose: _api_v2_pb2.Pose
    tool_velocity: _api_v2_pb2.Twist
    tool_acceleration: _api_v2_pb2.Twist
    joint_position_commands: _api_v2_pb2.Joints
    def __init__(self, header: _Optional[_Union[_api_v2_pb2.StateHeader, _Mapping]] = ..., joint_names: _Optional[_Iterable[str]] = ..., joint_positions: _Optional[_Union[_api_v2_pb2.Joints, _Mapping]] = ..., joint_velocities: _Optional[_Union[_api_v2_pb2.Joints, _Mapping]] = ..., joint_accelerations: _Optional[_Union[_api_v2_pb2.Joints, _Mapping]] = ..., joint_jerks: _Optional[_Union[_api_v2_pb2.Joints, _Mapping]] = ..., tool: _Optional[_Union[Tool, _Mapping]] = ..., tool_pose: _Optional[_Union[_api_v2_pb2.Pose, _Mapping]] = ..., tool_velocity: _Optional[_Union[_api_v2_pb2.Twist, _Mapping]] = ..., tool_acceleration: _Optional[_Union[_api_v2_pb2.Twist, _Mapping]] = ..., joint_position_commands: _Optional[_Union[_api_v2_pb2.Joints, _Mapping]] = ...) -> None: ...

class SystemState(_message.Message):
    __slots__ = ["header", "robot_controller_state", "rtos_state", "robot_state"]
    HEADER_FIELD_NUMBER: _ClassVar[int]
    ROBOT_CONTROLLER_STATE_FIELD_NUMBER: _ClassVar[int]
    RTOS_STATE_FIELD_NUMBER: _ClassVar[int]
    ROBOT_STATE_FIELD_NUMBER: _ClassVar[int]
    header: _api_v2_pb2.StateHeader
    robot_controller_state: RobotControllerState
    rtos_state: RTOSState
    robot_state: RobotState
    def __init__(self, header: _Optional[_Union[_api_v2_pb2.StateHeader, _Mapping]] = ..., robot_controller_state: _Optional[_Union[RobotControllerState, _Mapping]] = ..., rtos_state: _Optional[_Union[RTOSState, _Mapping]] = ..., robot_state: _Optional[_Union[RobotState, _Mapping]] = ...) -> None: ...

class TrajectoryPlayerCmd(_message.Message):
    __slots__ = ["header", "action"]
    HEADER_FIELD_NUMBER: _ClassVar[int]
    ACTION_FIELD_NUMBER: _ClassVar[int]
    header: _api_v2_pb2.CmdHeader
    action: TrajectoryPlayerAction
    def __init__(self, header: _Optional[_Union[_api_v2_pb2.CmdHeader, _Mapping]] = ..., action: _Optional[_Union[TrajectoryPlayerAction, str]] = ...) -> None: ...

class TrajectoryOverrideSpeedCmd(_message.Message):
    __slots__ = ["header", "percent"]
    HEADER_FIELD_NUMBER: _ClassVar[int]
    PERCENT_FIELD_NUMBER: _ClassVar[int]
    header: _api_v2_pb2.CmdHeader
    percent: float
    def __init__(self, header: _Optional[_Union[_api_v2_pb2.CmdHeader, _Mapping]] = ..., percent: _Optional[float] = ...) -> None: ...

class TrajectoryState(_message.Message):
    __slots__ = ["header", "uuid", "is_playable", "is_on_trajectory", "is_advancing", "player_state", "override_speed", "current_time", "duration"]
    HEADER_FIELD_NUMBER: _ClassVar[int]
    UUID_FIELD_NUMBER: _ClassVar[int]
    IS_PLAYABLE_FIELD_NUMBER: _ClassVar[int]
    IS_ON_TRAJECTORY_FIELD_NUMBER: _ClassVar[int]
    IS_ADVANCING_FIELD_NUMBER: _ClassVar[int]
    PLAYER_STATE_FIELD_NUMBER: _ClassVar[int]
    OVERRIDE_SPEED_FIELD_NUMBER: _ClassVar[int]
    CURRENT_TIME_FIELD_NUMBER: _ClassVar[int]
    DURATION_FIELD_NUMBER: _ClassVar[int]
    header: _api_v2_pb2.StateHeader
    uuid: str
    is_playable: bool
    is_on_trajectory: bool
    is_advancing: bool
    player_state: PlayerState
    override_speed: float
    current_time: float
    duration: float
    def __init__(self, header: _Optional[_Union[_api_v2_pb2.StateHeader, _Mapping]] = ..., uuid: _Optional[str] = ..., is_playable: bool = ..., is_on_trajectory: bool = ..., is_advancing: bool = ..., player_state: _Optional[_Union[PlayerState, str]] = ..., override_speed: _Optional[float] = ..., current_time: _Optional[float] = ..., duration: _Optional[float] = ...) -> None: ...

class RobotPose(_message.Message):
    __slots__ = ["pose", "joint_seeds"]
    POSE_FIELD_NUMBER: _ClassVar[int]
    JOINT_SEEDS_FIELD_NUMBER: _ClassVar[int]
    pose: _api_v2_pb2.Pose
    joint_seeds: _api_v2_pb2.Joints
    def __init__(self, pose: _Optional[_Union[_api_v2_pb2.Pose, _Mapping]] = ..., joint_seeds: _Optional[_Union[_api_v2_pb2.Joints, _Mapping]] = ...) -> None: ...

class Waypoint(_message.Message):
    __slots__ = ["motion_type", "robot_pose", "joint_positions", "name", "uuid", "velocity_scaling_percent", "blending_percent", "blending_speed_type", "pause_duration"]
    MOTION_TYPE_FIELD_NUMBER: _ClassVar[int]
    ROBOT_POSE_FIELD_NUMBER: _ClassVar[int]
    JOINT_POSITIONS_FIELD_NUMBER: _ClassVar[int]
    NAME_FIELD_NUMBER: _ClassVar[int]
    UUID_FIELD_NUMBER: _ClassVar[int]
    VELOCITY_SCALING_PERCENT_FIELD_NUMBER: _ClassVar[int]
    BLENDING_PERCENT_FIELD_NUMBER: _ClassVar[int]
    BLENDING_SPEED_TYPE_FIELD_NUMBER: _ClassVar[int]
    PAUSE_DURATION_FIELD_NUMBER: _ClassVar[int]
    motion_type: MotionType
    robot_pose: RobotPose
    joint_positions: _api_v2_pb2.Joints
    name: str
    uuid: str
    velocity_scaling_percent: float
    blending_percent: float
    blending_speed_type: BlendingSpeedType
    pause_duration: float
    def __init__(self, motion_type: _Optional[_Union[MotionType, str]] = ..., robot_pose: _Optional[_Union[RobotPose, _Mapping]] = ..., joint_positions: _Optional[_Union[_api_v2_pb2.Joints, _Mapping]] = ..., name: _Optional[str] = ..., uuid: _Optional[str] = ..., velocity_scaling_percent: _Optional[float] = ..., blending_percent: _Optional[float] = ..., blending_speed_type: _Optional[_Union[BlendingSpeedType, str]] = ..., pause_duration: _Optional[float] = ...) -> None: ...

class TrajectoryGenOptions(_message.Message):
    __slots__ = ["force_constant_speed", "desired_uuid"]
    FORCE_CONSTANT_SPEED_FIELD_NUMBER: _ClassVar[int]
    DESIRED_UUID_FIELD_NUMBER: _ClassVar[int]
    force_constant_speed: bool
    desired_uuid: str
    def __init__(self, force_constant_speed: bool = ..., desired_uuid: _Optional[str] = ...) -> None: ...

class MotionProfileConstraints(_message.Message):
    __slots__ = ["initial_position", "initial_velocity", "initial_acceleration", "final_position", "final_velocity", "final_acceleration", "max_speed", "max_acceleration", "max_deceleration", "max_jerk", "duration"]
    INITIAL_POSITION_FIELD_NUMBER: _ClassVar[int]
    INITIAL_VELOCITY_FIELD_NUMBER: _ClassVar[int]
    INITIAL_ACCELERATION_FIELD_NUMBER: _ClassVar[int]
    FINAL_POSITION_FIELD_NUMBER: _ClassVar[int]
    FINAL_VELOCITY_FIELD_NUMBER: _ClassVar[int]
    FINAL_ACCELERATION_FIELD_NUMBER: _ClassVar[int]
    MAX_SPEED_FIELD_NUMBER: _ClassVar[int]
    MAX_ACCELERATION_FIELD_NUMBER: _ClassVar[int]
    MAX_DECELERATION_FIELD_NUMBER: _ClassVar[int]
    MAX_JERK_FIELD_NUMBER: _ClassVar[int]
    DURATION_FIELD_NUMBER: _ClassVar[int]
    initial_position: float
    initial_velocity: float
    initial_acceleration: float
    final_position: float
    final_velocity: float
    final_acceleration: float
    max_speed: float
    max_acceleration: float
    max_deceleration: float
    max_jerk: float
    duration: float
    def __init__(self, initial_position: _Optional[float] = ..., initial_velocity: _Optional[float] = ..., initial_acceleration: _Optional[float] = ..., final_position: _Optional[float] = ..., final_velocity: _Optional[float] = ..., final_acceleration: _Optional[float] = ..., max_speed: _Optional[float] = ..., max_acceleration: _Optional[float] = ..., max_deceleration: _Optional[float] = ..., max_jerk: _Optional[float] = ..., duration: _Optional[float] = ...) -> None: ...

class Robot(_message.Message):
    __slots__ = ["brand", "model", "flange_to_tcp"]
    BRAND_FIELD_NUMBER: _ClassVar[int]
    MODEL_FIELD_NUMBER: _ClassVar[int]
    FLANGE_TO_TCP_FIELD_NUMBER: _ClassVar[int]
    brand: str
    model: str
    flange_to_tcp: _api_v2_pb2.Pose
    def __init__(self, brand: _Optional[str] = ..., model: _Optional[str] = ..., flange_to_tcp: _Optional[_Union[_api_v2_pb2.Pose, _Mapping]] = ...) -> None: ...

class TrajectoryGenInputs(_message.Message):
    __slots__ = ["waypoints", "robot", "options", "motion_profile_constaints"]
    WAYPOINTS_FIELD_NUMBER: _ClassVar[int]
    ROBOT_FIELD_NUMBER: _ClassVar[int]
    OPTIONS_FIELD_NUMBER: _ClassVar[int]
    MOTION_PROFILE_CONSTAINTS_FIELD_NUMBER: _ClassVar[int]
    waypoints: _containers.RepeatedCompositeFieldContainer[Waypoint]
    robot: Robot
    options: TrajectoryGenOptions
    motion_profile_constaints: _containers.RepeatedCompositeFieldContainer[MotionProfileConstraints]
    def __init__(self, waypoints: _Optional[_Iterable[_Union[Waypoint, _Mapping]]] = ..., robot: _Optional[_Union[Robot, _Mapping]] = ..., options: _Optional[_Union[TrajectoryGenOptions, _Mapping]] = ..., motion_profile_constaints: _Optional[_Iterable[_Union[MotionProfileConstraints, _Mapping]]] = ...) -> None: ...

class TrajectoryGenerationCmd(_message.Message):
    __slots__ = ["header", "inputs"]
    HEADER_FIELD_NUMBER: _ClassVar[int]
    INPUTS_FIELD_NUMBER: _ClassVar[int]
    header: _api_v2_pb2.CmdHeader
    inputs: TrajectoryGenInputs
    def __init__(self, header: _Optional[_Union[_api_v2_pb2.CmdHeader, _Mapping]] = ..., inputs: _Optional[_Union[TrajectoryGenInputs, _Mapping]] = ...) -> None: ...

class PathPreview(_message.Message):
    __slots__ = ["poses"]
    POSES_FIELD_NUMBER: _ClassVar[int]
    poses: _containers.RepeatedCompositeFieldContainer[_api_v2_pb2.Pose]
    def __init__(self, poses: _Optional[_Iterable[_Union[_api_v2_pb2.Pose, _Mapping]]] = ...) -> None: ...

class GenerationStatus(_message.Message):
    __slots__ = ["uuid", "result"]
    UUID_FIELD_NUMBER: _ClassVar[int]
    RESULT_FIELD_NUMBER: _ClassVar[int]
    uuid: str
    result: GenerationResult
    def __init__(self, uuid: _Optional[str] = ..., result: _Optional[_Union[GenerationResult, str]] = ...) -> None: ...

class TrajectoryGeneratorState(_message.Message):
    __slots__ = ["header", "state", "history"]
    HEADER_FIELD_NUMBER: _ClassVar[int]
    STATE_FIELD_NUMBER: _ClassVar[int]
    HISTORY_FIELD_NUMBER: _ClassVar[int]
    header: _api_v2_pb2.StateHeader
    state: GeneratorState
    history: _containers.RepeatedCompositeFieldContainer[GenerationStatus]
    def __init__(self, header: _Optional[_Union[_api_v2_pb2.StateHeader, _Mapping]] = ..., state: _Optional[_Union[GeneratorState, str]] = ..., history: _Optional[_Iterable[_Union[GenerationStatus, _Mapping]]] = ...) -> None: ...

class InputOutput(_message.Message):
    __slots__ = ["name", "io_type", "value_type", "value"]
    NAME_FIELD_NUMBER: _ClassVar[int]
    IO_TYPE_FIELD_NUMBER: _ClassVar[int]
    VALUE_TYPE_FIELD_NUMBER: _ClassVar[int]
    VALUE_FIELD_NUMBER: _ClassVar[int]
    name: str
    io_type: IoType
    value_type: IoValueType
    value: str
    def __init__(self, name: _Optional[str] = ..., io_type: _Optional[_Union[IoType, str]] = ..., value_type: _Optional[_Union[IoValueType, str]] = ..., value: _Optional[str] = ...) -> None: ...

class WaypointWithIos(_message.Message):
    __slots__ = ["waypoint", "ios"]
    WAYPOINT_FIELD_NUMBER: _ClassVar[int]
    IOS_FIELD_NUMBER: _ClassVar[int]
    waypoint: Waypoint
    ios: _containers.RepeatedCompositeFieldContainer[InputOutput]
    def __init__(self, waypoint: _Optional[_Union[Waypoint, _Mapping]] = ..., ios: _Optional[_Iterable[_Union[InputOutput, _Mapping]]] = ...) -> None: ...
