'''
MIT License

Copyright (c) 2024 Fuzzy Logic Robotics

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
'''

'''

Notes: 

Get the robot to a defined joint configuration

'''

import socket
import time
import sys
import os
sys.path.append('protos')
from protos import api_v2_rtos_pb2 as flr_rtos_api_v2
from protos import api_v2_pb2 as flr_api_v2 
from google.protobuf.any_pb2 import Any
from scipy.spatial.transform import Rotation as R


joint_config = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0]  # configuration target

# UDP server configuration
UDP_IP = "localhost" # RTOS IP
UPD_PORT = 51218     # flr encoded in alphabetical number but instead of 6 for 'f' it's 5
RTOS_ADRESS = (UDP_IP, UPD_PORT)

# create UDP socket for receiving
udp_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

# create UDP socket for sending
udp_socket_send = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

def send(message):
    msg = Any()                                 # creates protobug
    msg.Pack(message)                           # fills protobuf
    serial_msg = msg.SerializeToString()        # serialize the message to bytes
    udp_sock.sendto(serial_msg, RTOS_ADRESS)
    # print(f"Sent UDP message: {message}")


def receive( response_message_type):
    response_data, server = udp_sock.recvfrom(10024)    # receive data from the socket
    response_message = response_message_type()          # initialise the response message of the specified type
    response_message.ParseFromString(response_data)     # parse received data into the response message
    return response_message

def target_reached(target):
    sys_state = receive(flr_rtos_api_v2.SystemState)                # get system state
    current_config = sys_state.robot_state.joint_positions.value    # get current configuration

    if positions_match(current_config, target):                     # checks if two positions are equal with a small tolerance
        print("target reached")
        return True
    return False


def positions_match(current_pose, target_pose, tolerance=1e-2):
    
    if len(current_pose) != len(target_pose):       # Check if the length of both tuples is the same
        print("compared vectors must be the same length")
        return False
    
    for cp, te in zip(current_pose, target_pose):   # Compare each element in the tuples
        if abs(cp - te) > tolerance:
            return False
    
    return True

def main():
    try:
        # Note: send any message to be able to receive

        # enable jogging
        rtos_ctrl_mode_cmd = flr_rtos_api_v2.RTOSControlModeCmd(control_mode_request=flr_rtos_api_v2.RTOS_CONTROL_MODE_JOGGING)
        send(rtos_ctrl_mode_cmd) 

        # command definition
        joints = flr_api_v2.Joints()  # create Joints message
        joints.value.extend(joint_config)
        cmd = flr_rtos_api_v2.JointPositionJoggingCmd(joint_positions=joints)

        # command loop
        while not target_reached(joint_config):
            send(cmd)
            time.sleep(0.1)
            target_reached(joint_config)
        
    finally:
        udp_sock.close()    # close udp socket 


if __name__=="__main__":
   main()

