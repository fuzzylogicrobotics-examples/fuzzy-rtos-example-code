'''
MIT License

Copyright (c) 2024 Fuzzy Logic Robotics

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
'''

'''
Jog the robot axis with the keyboard
'''

import json
import paho.mqtt.client as paho
import time
import keyboard

####Things to change
rtos_IP="localhost" #IP adress of the RTOS IPC 
speed = 0.03*10 #Value that determines the joint speed command
####

port= 1885
ctrl_mode_topic = "/fuzzy_studio/rtos_control_mode_command"
joint_vel_topic = "/fuzzy_studio/joint_velocity_jogging_command"

joint_vel_json = {"jointVelocities":{"value":[0,0,0,0,0,0]}}  #init json jogging message

rate = 0.01 #100 Hz

#initialisation client
client = paho.Client(paho.CallbackAPIVersion.VERSION1, "ClientSender")       #create client object
client.connect(rtos_IP, port)           #establish connection

print("startLoop")

while(True):
    joint_vel_json["jointVelocities"]["value"][0] = 0 #reinitialise the jogging message
    joint_vel_json["jointVelocities"]["value"][1] = 0
    joint_vel_json["jointVelocities"]["value"][2] = 0
    joint_vel_json["jointVelocities"]["value"][3] = 0
    joint_vel_json["jointVelocities"]["value"][4] = 0
    joint_vel_json["jointVelocities"]["value"][5] = 0
    if keyboard.is_pressed('a'):
        joint_vel_json["jointVelocities"]["value"][0] = speed
    if keyboard.is_pressed('q'):
        joint_vel_json["jointVelocities"]["value"][0] = -speed
    if keyboard.is_pressed('z'):
        joint_vel_json["jointVelocities"]["value"][1] = speed
    if keyboard.is_pressed('s'):
        joint_vel_json["jointVelocities"]["value"][1] = -speed
    if keyboard.is_pressed('e'):
        joint_vel_json["jointVelocities"]["value"][2] = speed
    if keyboard.is_pressed('d'):
        joint_vel_json["jointVelocities"]["value"][2] = -speed
    if keyboard.is_pressed('r'):
        joint_vel_json["jointVelocities"]["value"][3] = speed
    if keyboard.is_pressed('f'):
        joint_vel_json["jointVelocities"]["value"][3] = -speed
    if keyboard.is_pressed('t'):
        joint_vel_json["jointVelocities"]["value"][4] = speed
    if keyboard.is_pressed('g'):
        joint_vel_json["jointVelocities"]["value"][4] = -speed
    if keyboard.is_pressed('y'):
        joint_vel_json["jointVelocities"]["value"][5] = speed
    if keyboard.is_pressed('h'):
        joint_vel_json["jointVelocities"]["value"][5] = -speed
    if keyboard.is_pressed('p'):
        break
    client.publish(joint_vel_topic, json.dumps(joint_vel_json))  #publish jogging velocity goal
    client.publish(ctrl_mode_topic, json.dumps({"controlModeRequest":"RTOS_CONTROL_MODE_JOGGING"})) #Set RTOS state to jogging in order to start moving the robot
    if keyboard.is_pressed('w'):
        break
    time.sleep(rate)
client.publish(ctrl_mode_topic, json.dumps({"controlModeRequest":"RTOS_CONTROL_MODE_IDLE"})) #Set RTOS state to idle in order to stop it from moving

# quit