'''
MIT License

Copyright (c) 2024 Fuzzy Logic Robotics

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
'''

'''
Send a trajectory to RTOS
Define the trajectory in the corresponding json file

To run this script, launch an instance of Fuzzy RTOS runtime sim. Then, in the Robot Control Panel in Fuzzy Studio, switch to physical and connect to Fuzzy RTOS in localhost. You can then send the trajectory.
'''

import json
import paho.mqtt.client as paho
import time
import os
import signal
import sys


##### Things to change
rtos_IP="localhost"                                   # IP adress of the RTOS IPC 
traj_cmd_name = "example_traj_gen_cmd_rtos_only.json" # traj file name
#####

# Get the current working directory
current_directory = os.getcwd()
# Construct the full file path
traj_cmd_path = os.path.join(current_directory, traj_cmd_name)

port= 1885 #port of the MQTT broker in the IPC
player_command_topic = "/fuzzy_studio/trajectory_player_command"
traj_generation_command_topic = "/fuzzy_studio/trajectory_generation_command"
rtos_ctrl_mode_cmd_topic = "/fuzzy_studio/rtos_control_mode_command"
system_state_topic = "system_state"
traj_state_topic = "trajectory_state"

sys_state_received = False
traj_state_received = False
sys_state = []
traj_state = []
traj_cmd = []

def main() :
    global traj_cmd, traj_state, sys_state
    #initialisation client
    client = paho.Client(paho.CallbackAPIVersion.VERSION2, "ClientSender")       #create client object
    client.on_subscribe = on_subscribe       #assign function to callback
    client.on_message = on_message        #assign function to callback
    client.on_disconnect = on_disconnect

    def signal_handler(sig, frame):
        print('You pressed Ctrl+C! Stopping The robot before exiting')
        client.publish(rtos_ctrl_mode_cmd_topic, json.dumps({"controlModeRequest": "RTOS_CONTROL_MODE_IDLE"}))
        sys.exit(0)
        
    signal.signal(signal.SIGINT, signal_handler)

    client.connect(rtos_IP, port)           #establish connection
    client.loop_start()
    client.subscribe(system_state_topic)
    client.subscribe(traj_state_topic)
    
    if(not os.path.isfile(traj_cmd_path)):
       print("invalid file path")
       return
    
    traj_cmd_file = open(traj_cmd_path)
    traj_cmd = json.load(traj_cmd_file)

    waitForState()
    print("Robot state : " + json.dumps(sys_state["robotState"]))

    client.publish(traj_generation_command_topic, json.dumps(traj_cmd))

    if(TrajIsReady()):
        client.publish(player_command_topic, json.dumps({"action": "TRAJECTORY_PLAYER_ACTION_PLAY"}))
        time.sleep(0.3)
        while(traj_state["playerState"] != "PLAYER_STATE_FINISHED"):
           time.sleep(0.02)
    else :
       print("This Trajectory is not playable")
       client.publish(rtos_ctrl_mode_cmd_topic, json.dumps({"controlModeRequest": "RTOS_CONTROL_MODE_IDLE"}))
                   

def on_subscribe(client, userdata, mid, granted_qos, properties=None):   #create function for callback
   print("subscribed with qos", granted_qos, "\n")
   pass

def on_message(client, userdata, message):
    global sys_state_received, traj_state_received
    global sys_state, traj_state
    if (message.topic == system_state_topic):
        sys_state_received = True
        sys_state = json.loads(message.payload)
    if(message.topic == traj_state_topic):
       traj_state_received = True
       traj_state = json.loads(message.payload)
    pass

def on_log(client, userdata, level, buf):
   print("log: ", buf)

def on_disconnect(client, userdata, rc):
   print("client disconnected ok") 

def waitForState():
   print("waiting for state")
   while(sys_state_received == False):
      time.sleep(0.02)

def TrajIsReady():
    # timeouts
    # print traj generee ou pas
    while(traj_state_received == False):
       time.sleep(0.02)
    while(traj_cmd["inputs"]["options"]["desiredUuid"] != traj_state["uuid"]):
        time.sleep(0.02)
    if(not traj_state["isPlayable"]): 
       print("This trajectory is not playable")
       return False
    if (not traj_state["isOnTrajectory"]):
       print("The robot is not on the Trajectory")
       return False
    print("Trajectory is ready")
    return True




if __name__=="__main__":
   main()