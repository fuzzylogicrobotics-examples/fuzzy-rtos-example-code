"""
MIT License

Copyright (c) 2024 Fuzzy Logic Robotics

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

"""
Moves the robot to a given Pose
"""

import json
import paho.mqtt.client as paho
import keyboard
import time


fuzzy_rtos_ip = "localhost"
cartesian_target_pos = [0.600, 0.0, 0.500]
speed = 80 # percentage of 250 mm/s

fuzzy_rtos_mqtt_port = 1885
ctrl_mode_topic = "/fuzzy_studio/rtos_control_mode_command"
cart_pose_topic = "/fuzzy_studio/cartesian_pose_jogging_command"
jogg_speed_topic = "/fuzzy_studio/jogging_speed_override_command"


# initialize json jogging message with a default pose
cart_pose_json = {
    "desiredPose": {
        "translation": {"x": 0, "y": 0, "z": 0},
        "rotation": {"qx": 1, "qy": 0.0, "qz": 0.0, "qw": 0.0},
    }
}

# MQTT client initialization
client = paho.Client(paho.CallbackAPIVersion.VERSION2, "ClientSender")
client.connect(fuzzy_rtos_ip, fuzzy_rtos_mqtt_port)
# declares the jogging speed to RTOS
client.publish(jogg_speed_topic, json.dumps({"percent": speed}))

# fill the json message
cart_pose_json["desiredPose"]["translation"]["x"] = cartesian_target_pos[0]
cart_pose_json["desiredPose"]["translation"]["y"] = cartesian_target_pos[1]
cart_pose_json["desiredPose"]["translation"]["z"] = cartesian_target_pos[2]

print(" Cartesian target:", cartesian_target_pos)
print(" Speed:", speed, "%")
print(" Press q to quit")
while True:
    # start jogging
    client.publish(
        ctrl_mode_topic, json.dumps({"controlModeRequest": "RTOS_CONTROL_MODE_JOGGING"})
    )
    # publish jogging target
    client.publish(cart_pose_topic, json.dumps(cart_pose_json))
    if keyboard.is_pressed("q"):
        break
    time.sleep(0.01)

# stop jogging
client.publish(
    ctrl_mode_topic, json.dumps({"controlModeRequest": "RTOS_CONTROL_MODE_IDLE"})
)
