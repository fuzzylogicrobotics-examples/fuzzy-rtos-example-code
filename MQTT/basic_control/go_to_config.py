"""
MIT License

Copyright (c) 2024 Fuzzy Logic Robotics

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

"""
Moves the robot the a given joint configuration
"""

import json
from scipy.spatial.transform import Rotation as R
import paho.mqtt.client as paho
import time
import keyboard

fuzzy_rtos_ip = "localhost"

# joint configuration target in rad
joint_target = [
    0.0,
    0.0,
    1.57079632,
    0.0,
    -1.57079632,
    1.57079632,
]
speed = 90  # percentage of 250 mm/s

fuzzy_rtos_port = 1885  # Fuzzy RTOS IPC MQTT port
ctrl_mode_topic = "/fuzzy_studio/rtos_control_mode_command"
joint_pos_topic = "/fuzzy_studio/joint_position_jogging_command"
jogg_speed_topic = "/fuzzy_studio/jogging_speed_override_command"
# init json jogging message
joint_pos_json = {"joint_positions": {"value": [0, 0, 0, 0, 0, 0]}}

# MQTT client initialization
client = paho.Client(paho.CallbackAPIVersion.VERSION1, "ClientSender")
client.connect(fuzzy_rtos_ip, fuzzy_rtos_port)
# declared jogging speed to Fuzzy RTOS
client.publish(jogg_speed_topic, json.dumps({"percent": speed}))

for i in range(len(joint_target)):  # filling json with the target joint values
    joint_pos_json["joint_positions"]["value"][i] = joint_target[i]

print(" Joint target:", joint_target)
print(" Speed:", speed, "%")
print(" Press q to quit")

while True:
    client.publish(joint_pos_topic, json.dumps(joint_pos_json))  # publish jogging goal
    # Set RTOS state to jogging in order to start moving the robot
    client.publish(
        ctrl_mode_topic,
        json.dumps({"controlModeRequest": "RTOS_CONTROL_MODE_JOGGING"}),
    )
    if keyboard.is_pressed("q"):
        break
    time.sleep(0.01)

# Set RTOS state to idle in order to stop it from moving
client.publish(
    ctrl_mode_topic,
    json.dumps({"controlModeRequest": "RTOS_CONTROL_MODE_IDLE"}),
)
