'''
MIT License

Copyright (c) 2024 Fuzzy Logic Robotics

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
'''

'''
Print the system state
'''

import json
import paho.mqtt.client as paho
import time
import os
import signal
import sys


#####Things to change
rtos_IP="localhost" #IP adress of the RTOS IPC 
#####

port= 1885 #port of the MQTT broker in the IPC
system_state_topic = "system_state"

sys_state_received = False
sys_state = []

def main() :
    global sys_state
    #initialisation client
    client = paho.Client(paho.CallbackAPIVersion.VERSION2, "ClientSender")       #create client object
    client.on_subscribe = on_subscribe       #assign function to callback
    client.on_publish = on_publish        #assign function to callback
    client.on_message = on_message        #assign function to callback
    client.on_disconnect = on_disconnect
        
    client.connect(rtos_IP, port)           #establish connection
    client.loop_start()                      #start MQTT subscribe loop
    client.subscribe(system_state_topic)     #subscribe to system state

    waitForState()  #wait until robot_state is received 
    print("Robot state : " + json.dumps(sys_state["robotState"],indent=4)) #print robot_state
                   
def on_subscribe(client, userdata, mid, rc, granted_qos):   #create function for callback
   print("subscribed with qos", granted_qos, "\n") # google le
   pass

def on_message(client, userdata, message):
    global sys_state_received, sys_state
    if (message.topic == system_state_topic):
        sys_state_received = True
        sys_state = json.loads(message.payload) #load robot state
    pass

def on_log(client, userdata, level, buf):
   print("log: ", buf)

def on_publish(client,userdata, mid):   #create function for callback
   # print("data published mid =", mid, "\n")
   pass

def on_disconnect(client, userdata, rc):
   print("client disconnected ok") 

def waitForState():
   print("waiting for state")
   while(sys_state_received == False):
      time.sleep(0.02)

if __name__=="__main__":
   main()