'''
MIT License

Copyright (c) 2024 Fuzzy Logic Robotics

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
'''

'''
Linear Jogging with directional arrows
'''

import json
import paho.mqtt.client as paho
import time
import keyboard

####Things to change
rtos_IP="localhost" #IP adress of the RTOS IPC 
speed = 0.03 #Value that determines the cartesian speed command
####

port= 1885 #port of the MQTT broker in the IPC (always 1885)
ctrl_mode_topic = "/fuzzy_studio/rtos_control_mode_command"
cart_vel_topic = "/fuzzy_studio/cartesian_velocity_jogging_command"
cart_vel_json = {"desiredVelocity":{"linear":{"x":0,"y":0,"z":0},"angular":{"x":0,"y":0,"z":0}}} #init json jogging message

rate = 0.01 #100 Hz

#initialisation client
client = paho.Client(paho.CallbackAPIVersion.VERSION1, "ClientSender")       #create client object
client.connect(rtos_IP, port)           #establish connection

print("startLoop")
print("press q to quit")

while(True):
    cart_vel_json["desiredVelocity"]["linear"]["x"] = 0 #reinitialise the jogging message
    cart_vel_json["desiredVelocity"]["linear"]["y"] = 0
    cart_vel_json["desiredVelocity"]["linear"]["z"] = 0
    if keyboard.is_pressed('up'):
        cart_vel_json["desiredVelocity"]["linear"]["x"] = speed 
    if keyboard.is_pressed('down'):
        cart_vel_json["desiredVelocity"]["linear"]["x"] = -speed
    if keyboard.is_pressed('right'):
        cart_vel_json["desiredVelocity"]["linear"]["y"] = speed
    if keyboard.is_pressed('left'):
        cart_vel_json["desiredVelocity"]["linear"]["y"] = -speed
    if keyboard.is_pressed('q'):
        break
    client.publish(cart_vel_topic, json.dumps(cart_vel_json))   #publish jogging velocity goal
    client.publish(ctrl_mode_topic, json.dumps({"controlModeRequest":"RTOS_CONTROL_MODE_JOGGING"})) #Set RTOS state to jogging in order to start moving the robot
    time.sleep(rate)
client.publish(ctrl_mode_topic, json.dumps({"controlModeRequest":"RTOS_CONTROL_MODE_IDLE"})) #Set RTOS state to idle in order to stop it from moving

