# Receive boolean Fuzzy Sudio IO

## What this sample code does

Receive a boolean output from Fuzzy Studio with this sample python script.
This can help you get started on Fuzzy Studio IOs if you have never used them. If you are not familiar with the MQTT communication protocol which is what we use for Fuzzy Studio IOs, this can also help you get started on that.

[For more information on how to use MQTT with python, please visit the paho-mqtt documentation.](https://pypi.org/project/paho-mqtt/)

To learn more about Fuzzy Studio IOs and what they are, [click here](https://fuzzylogicrobotics.gitlab.io/fuzzy_docs/docs/4.11.0/Fuzzy%20Studio/Trajectories%20panel/IO%20Management/).


## How to use this sample code 

First, you need to define the corresponding exchange table in Fuzzy Studio. For this you can directly import the [boolean_output_device.fsiodev](https://gitlab.com/fuzzylogicrobotics-examples/fuzzy-rtos-example-code/-/blob/main/MQTT/scripts_using_fuzzy_studio_ios/read_boolean_io_from_fuzzy_studio/boolean_output_device.fsiodev?ref_type=heads) file from this folder into Fuzzy Studio's exchange tables.
To learn more about Fuzzy Studio exchange tables and how to import one, [click here](https://fuzzylogicrobotics.gitlab.io/fuzzy_docs/docs/4.11.0/Fuzzy%20Studio/Trajectories%20panel/IO%20Management/Setting%20up%20IOs).

Then you will want to associate the output to one or several trajectory waypoints. 

Run the python script from your computer. When you play the trajectory, the ouput will be published on the MQTT topic and the boolean value will be printed on your terminal.
