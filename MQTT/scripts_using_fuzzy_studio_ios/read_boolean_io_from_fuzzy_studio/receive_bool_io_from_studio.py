'''
MIT License

Copyright (c) 2024 Fuzzy Logic Robotics

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
'''

'''
Receive and print the value of a bool IO from Fuzzy Studio
Import the provided device or create your own. Don't forget the change the mqtt topic name to match the name of you device.
To read the value, you must send a value from studio by associating the IO to a trajectory WP, forcing the value, or sending a test directly
from the exchange table.

'''

import paho.mqtt.client as mqtt

mqtt_broker = 'localhost'
mqtt_port = 1883    # port used by Fuzzy Studio
mqtt_topic = '/fuzzy_studio/io/plc/to_device'   # topic name on which Fuzzy Studio publihes, can be found on the device window in the exchange tables

def on_connect(client, userdata, flags, reason_code, properties):
    print("Connected with result code " + str(reason_code))
    # Subscribe to the topic
    client.subscribe(mqtt_topic)

# when message is received
def on_message(client, userdata, message):
    # if the io is a boolean
    boolean_value = bool(message.payload[0])
    print(f"Received boolean: {boolean_value}")

def on_subscribe(client, userdata, mid, rc, granted_qos):
    print("subscribed successfully")

# Create an MQTT client instance
client = mqtt.Client(mqtt.CallbackAPIVersion.VERSION2)

# Assign the callback functions
client.on_connect = on_connect
client.on_message = on_message
client.on_subscribe = on_subscribe

# Connect to the MQTT broker
client.connect(mqtt_broker, mqtt_port)

# Start the loop to process callbacks and handle reconnections and messages
client.loop_forever()