# Basic trajectory control ui with Fuzzy RTOS MQTT API using Fuzzy Studio exchange table

**This example allows you to select a trajectory between the two first trajectory on your project, then manage and monitor this trajectory, using IOs from Fuzzy Studio exchange table**

Refer to the [IO management](https://fuzzylogicrobotics.gitlab.io/fuzzy_docs/docs/4.10.0/Fuzzy%20Studio/Trajectories%20panel/IO%20Management/) section of the documentation for more information on how to use the exchange table:
- [How to set up IOs](https://fuzzylogicrobotics.gitlab.io/fuzzy_docs/docs/4.10.0/Fuzzy%20Studio/Trajectories%20panel/IO%20Management/Setting%20up%20IOs)
- [How to associate IOs to trajectories](https://fuzzylogicrobotics.gitlab.io/fuzzy_docs/docs/4.10.0/Fuzzy%20Studio/Trajectories%20panel/IO%20Management/Adding%20IOs%20to%20trajectories)
- [How to monitor IOs](https://fuzzylogicrobotics.gitlab.io/fuzzy_docs/docs/4.10.0/Fuzzy%20Studio/Trajectories%20panel/IO%20Management/Monitoring%20IOs)

The UI is achived with Qt PyQt5. Feel free to modify the ui file with Qt Creator (free).

## Usage instructions

To successfully run this code, you will need to implement the corresponding exchange table into your Fuzzy Studio project. For this, import the IO device provided in the repository (io_device_exchange_table.fsiodev) directly into Fuzzy Studio.

