#!/usr/bin/env python
'''
MIT License

Copyright (c) 2024 Fuzzy Logic Robotics

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
'''

'''

Connect to a broker, select a trajectory and control it via a simple RTOS GUI
Important: the exchange table must correspond:

from studio to device:
- Trajectory state
- Active trajectory
- Action error

from device to studio:
- Trajectory Actions (from Play to Rewind)



'''

import paho.mqtt.client as paho
import time
import sys
import os
import threading
from PyQt5.QtWidgets import QApplication, QMainWindow, QDialog
from PyQt5.QtWidgets import QApplication
from PyQt5.uic import loadUi
from PyQt5.QtGui import QIcon
from enum import Enum


global mainWindow

# Get current directory path
current_dir = os.getcwd()

class TrajectoryState(Enum):
    unknown = -1
    stopped = 0
    paused = 1
    playing = 2
    rewinding = 5
    pending_stop = 6  # Noms de constantes avec des majuscules et des underscores (snake_case) pour la lisibilité
    finished = 7
    not_on_trajectory = 8

    def __str__(self):
        return self.name.replace("_", " ").title()

    @classmethod
    def from_value(cls, value):
        for state in cls:
            if state.value == value:
                return state
        raise ValueError("No such value in State enum")
    
class Ui_Dialog(QDialog):
    def __init__(self, mainwindow):
        super().__init__()
        loadUi('popup.ui', self)

        self.mainwindow = mainwindow
        self.confirm_approach_button.clicked.connect(self.confirm_approach)
        self.cancel_approach_button.clicked.connect(self.cancel_approach)        
    
    def cancel_approach(self):
        print("cancel approach")
        self.mainwindow.approach_caution_acknowledged = False
        self.close()
    
    def confirm_approach(self):
        print("confirm approach")
        self.mainwindow.approach_caution_acknowledged = True
        self.close()


class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        loadUi(os.path.join(current_dir, 'rtos_gui.ui'), self)
        
        self.traj_cmd_topic = "/fuzzy_studio/io/gui_python/from_device" # publish toward studio
        self.traj_state_topic = "/fuzzy_studio/io/gui_python/to_device" # receive from studio

        self.Stack.setCurrentIndex(0)
        self.stop_event = threading.Event()
        self.loop_active = False
        self.approach_caution_acknowledged = False

        # broker connection pages
        self.BrokerIP.returnPressed.connect(lambda : self.connect_button.setEnabled(self.BrokerIP.text() != '')) # enables connect button if ip adresses is entered
        self.connect_button.clicked.connect(self.connect_to_broker)

        self.client = paho.Client(paho.CallbackAPIVersion.VERSION1)             # Create an MQTT client
        # self.client = paho.Client()             # old version of paho-mqtt

        self.client.on_connect = self.on_connect
        self.client.on_message = self.on_message

        self.close_button.clicked.connect(self.close)    # close GUI
        self.next_button.clicked.connect(self.go_to_control)

        # select traj
        self.trajectory1.clicked.connect(lambda: self.send_io_via_button(bytes([0, 1])))  
        self.trajectory2.clicked.connect(lambda: self.send_io_via_button(bytes([0, 2])))  

        # initialisation
        self.traj_state_display.setText('No trajectory playing')
        self.traj_state_display2.setText('No trajectory playing')
        self.play_button.setIcon(QIcon(os.path.join(current_dir, 'play.png')))      
        self.pause_button.setIcon(QIcon(os.path.join(current_dir, 'pause.png')))
        self.stop_button.setIcon(QIcon(os.path.join(current_dir, 'stop.png')))
        self.rewind_button.setIcon(QIcon(os.path.join(current_dir, 'rewind.png')))     
        
        self.play_button.clicked.connect(lambda: self.send_io_via_button(bytes([128, 0])))                # 1 = Play
        self.play_with_approach_button.clicked.connect(lambda: self.send_io_via_button(bytes([64, 0])))   # 2 = PlayWithApproach
        self.stop_button.clicked.connect(lambda: self.send_io_via_button(bytes([32, 0])))                 # 3 = Stop
        self.pause_button.clicked.connect(lambda: self.send_io_via_button(bytes([16, 0])))                # 4 = Pause
        self.rewind_button.clicked.connect(lambda: self.send_io_via_button(bytes([8, 0])))                # 5 = Rewind
        self.loop_button.clicked.connect(self.start_loop)                                    
        self.previous_button.clicked.connect(self.go_to_select_a_trajectory)

    def caution_popup(self):
        self.popup = Ui_Dialog(self)
        self.popup.exec_()  # block mainwindow 
        #self.popup.show()
        
    def on_connect(self, client, userdata, flags, rc):
        print("Connected with result code "+str(rc))
        self.client.subscribe(self.traj_state_topic)

    def on_message(self, client, userdata, message):
        # tokenize message payload
        bytes_data = [bytes([byte]) for byte in message.payload]

        # trajectory state display
        state_int = int.from_bytes(bytes_data[0])
        state = TrajectoryState(state_int)
        self.traj_state_display.setText(str(state))
        self.traj_state_display2.setText(str(state))

        # active trajectory display
        active_traj_int = int.from_bytes(bytes_data[1])

        if active_traj_int == 1:
            self.active_traj_display.setText("Dégagement")

        elif active_traj_int == 2:
            self.active_traj_display.setText("Ponçage")

    def stop_loop(self):
        self.stop_event.set()
        self.loop_active = False
        print("loop stopped")
        
    def start_loop(self):
        print("loop start")
        self.loop_active = True
        if self.stop_event.is_set(): # reset stop event to make it possible to loop again if the loop has been stopped
            self.stop_event.clear()
        thread = threading.Thread(target=self.loop_traj)
        thread.start()

    def loop_traj(self):
        self.client.publish(self.traj_cmd_topic, bytes([64, 0]))   # Approach
        time.sleep(5)
        while not self.stop_event.is_set():
            self.client.publish(self.traj_cmd_topic, bytes([64, 0]))  # Play with Approach
            time.sleep(0.5)

    def connect_to_broker(self):
        self.connect_button.setEnabled(False)
        self.BrokerIP.setEnabled(False)

        ip = self.BrokerIP.text()
        self.client.connect_async("localhost", 1883)  # Connect to an MQTT broker
        self.client.loop_start()             # Start the MQTT client loop

        print("subscribing to ",self.traj_state_topic)  # subscribe to trajectory state
        self.client.subscribe(self.traj_state_topic)
        self.go_to_select_a_trajectory()    # move to next page

    def send_io_via_button(self, msg):
        if msg == bytes([64, 0]):  # if approach button is pressed, popup window to ensure that approach is free of collisions before continuing
            self.caution_popup()
            time.sleep(5)
            if self.approach_caution_acknowledged == False: # cancel the approach
                return -1

        if self.loop_active:    # if trajectory loop is active, clicking any other button stops the looping thread
            self.stop_loop()
        self.client.publish(self.traj_cmd_topic, msg)

    def go_to_select_a_trajectory(self):
        if self.loop_active:    # if previous button was used and the trajectory was looping, stop the loop
            self.client.publish(self.traj_cmd_topic, bytes([32, 0]))   # stop trajectory
            self.stop_loop()    # stop the looping
        self.Stack.setCurrentIndex(1)

    def go_to_control(self):
        self.Stack.setCurrentIndex(2)
        self.close_button.setEnabled(True) 


if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = MainWindow()
    window.show()
    sys.exit(app.exec_())
            

