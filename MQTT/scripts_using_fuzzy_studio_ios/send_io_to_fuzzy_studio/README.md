# Send Int8 input to Fuzzy Sudio through Fuzzy Studio exchange tables

## What this sample code does

Send an int8 input to Fuzzy Studio with this sample python script.
This can help you get started on Fuzzy Studio IOs if you have never used them. If you are not familiar with the MQTT communication protocol which is what we use for Fuzzy Studio IOs, this can also help you get started on that.

[For more information on how to use MQTT with python, please visit the paho-mqtt documentation.](https://pypi.org/project/paho-mqtt/)

To learn more about Fuzzy Studio IOs and what they are, [click here](https://fuzzylogicrobotics.gitlab.io/fuzzy_docs/docs/4.11.0/Fuzzy%20Studio/Trajectories%20panel/IO%20Management/).


## How to use this sample code 

First, you need to define the corresponding exchange table in Fuzzy Studio. For this you can directly import the [input_exchange_table.fsiodev](https://gitlab.com/fuzzylogicrobotics-examples/fuzzy-rtos-example-code/-/blob/dev/MQTT/scripts_using_fuzzy_studio_ios/send_io_to_fuzzy_studio/input_exchange_table.fsiodev?ref_type=heads) file from this folder into Fuzzy Studio's exchange tables.
To learn more about Fuzzy Studio exchange tables and how to import one, [click here](https://fuzzylogicrobotics.gitlab.io/fuzzy_docs/docs/4.11.0/Fuzzy%20Studio/Trajectories%20panel/IO%20Management/Setting%20up%20IOs).

Then you will want to associate the input to one or several trajectory waypoints and associate a value. Fuzzy Studio will wait for the variable to take that value to continue the trajectory.

