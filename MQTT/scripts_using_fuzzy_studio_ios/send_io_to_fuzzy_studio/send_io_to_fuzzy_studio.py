'''
MIT License

Copyright (c) 2024 Fuzzy Logic Robotics

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
'''

'''
Send an integer value to a given topic on Fuzzy Studio periodically
Import the provided device or create your own. Don't forget to change the MQTT topic name to match the name of your device.
'''

import paho.mqtt.client as mqtt
import schedule
import time
import numpy as np

mqtt_broker = 'localhost'
mqtt_port = 1883    # port used by Fuzzy Studio
mqtt_topic = '/fuzzy_studio/io/PLC/from_device'   # topic name to which you will publish the integer value, can be found on the device window in the exchange tables

# Create an MQTT client instance
client = mqtt.Client(mqtt.CallbackAPIVersion.VERSION2)

def on_connect(client, userdata, flags, reason_code, properties):
    print("Connected with result code " + str(reason_code))

def on_publish(client, userdata, mid, tes, content):
    print("Message published successfully")

def publish_integer():
    integer_value = np.uint8(10) # Replace this with the integer value you want to send
    client.publish(mqtt_topic, bytes([integer_value]))  # data is sent in bytes
    print(f"Published integer: {bytes([integer_value])}")

# Assign the callback functions
client.on_connect = on_connect
client.on_publish = on_publish

# Connect to the MQTT broker
client.connect(mqtt_broker, mqtt_port)

# Schedule the publish_integer function to run periodically
schedule.every(5).seconds.do(publish_integer)  # Change the interval as needed

# Function to keep the schedule running
def run_scheduler():
    while True:
        schedule.run_pending()
        time.sleep(1)

# Start the loop to process callbacks and handle reconnections and messages
client.loop_start()

# Run the scheduler to publish periodically (keep the following line commented to publish only once)
# run_scheduler()

# to publish only once
publish_integer()