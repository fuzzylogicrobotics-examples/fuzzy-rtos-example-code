#include <csignal>
#include <utility>

#include <google/protobuf/any.pb.h>
#include <google/protobuf/message.h>
#include <protos/api_v2_rtos.pb.h>

#if defined(_WIN32)
#include <WS2tcpip.h>
#include <winsock2.h>
#else
#include <fcntl.h>
#include <unistd.h>

#include <arpa/inet.h>
#include <linux/errqueue.h>
#include <linux/net_tstamp.h>
#include <linux/sockios.h>
#include <net/if.h>
#include <netinet/tcp.h>
#include <sys/ioctl.h>
#include <sys/types.h>

#define SOCKET int
#define SOCKET_ERROR -1
#endif

namespace
{

std::atomic<bool> keep_running = true;

int getSocketError()
{
#if defined(_WIN32)
    return WSAGetLastError();
#else
    return errno;
#endif
}

google::protobuf::Any asAny(google::protobuf::Message& msg)
{
    google::protobuf::Any ret;
    ret.PackFrom(msg);
    return ret;
}

std::error_code returnValueToErrorCode(int code)
{
    return {code == SOCKET_ERROR ? getSocketError() : 0, std::system_category()};
}

std::error_code setRecvTimeout(SOCKET socket, std::chrono::microseconds timeout)
{
#if defined(__linux__)
    struct timeval tv;
    auto seconds = std::chrono::floor<std::chrono::duration<time_t>>(timeout);
    tv.tv_sec = seconds.count();
    tv.tv_usec = std::chrono::duration_cast<std::chrono::microseconds>(timeout - seconds).count();
    int ret = setsockopt(socket, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv));
#elif defined(_WIN32)
    const DWORD dw_timeout = std::chrono::ceil<std::chrono::duration<DWORD, std::milli>>(timeout).count();

    int ret = setsockopt(socket, SOL_SOCKET, SO_RCVTIMEO, reinterpret_cast<const char*>(&dw_timeout), sizeof(dw_timeout));
#endif
    return returnValueToErrorCode(ret);
}

int resetDriver(SOCKET s, const sockaddr_in& dest)
{
    flr_api::v2::rtos::RTOSDriverInitializationCmd rtos_init_cmd;
    std::cout << "Resetting driver\n";
    const auto payload = asAny(rtos_init_cmd).SerializeAsString();
    return sendto(s, payload.data(), static_cast<int>(payload.size()), 0, reinterpret_cast<const sockaddr*>(&dest), sizeof(dest));
}

void sigHandler(int /* sig */)
{
    keep_running = false;
}

enum class EScenario : int
{
    RobotOnly = 0,
    ExtAxisOnly,
    CombinedAxes
};

} // namespace

int main(int, char**)
{
#if defined(_WIN32)
    WSAData data{};
    WSAStartup(MAKEWORD(2, 2), &data);
#endif

    std::array<char, 8192> recv_data_buf{};

    signal(SIGINT, sigHandler);

    sockaddr_in local{};
    local.sin_family = AF_INET;
    inet_pton(AF_INET, "0.0.0.0", &local.sin_addr.s_addr);
    local.sin_port = 0;

    sockaddr_in dest{};
    dest.sin_family = AF_INET;
    inet_pton(AF_INET, "127.0.0.1", &dest.sin_addr.s_addr);
    dest.sin_port = htons(51218);

    SOCKET s = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);

    if(const auto ec = setRecvTimeout(s, std::chrono::seconds(2)); ec)
    {
        std::cerr << "Failed to set recv timeout: " << ec.message() << std::endl;
        return -1;
    }

    if(const auto ret = bind(s, reinterpret_cast<const sockaddr*>(&local), sizeof(local)); ret < 0)
    {
        std::cerr << "Failed to bind socket: " << returnValueToErrorCode(ret).message() << std::endl;
        return -1;
    }

    // Send an empty driver initialization message to reset the driver, and so that Fuzzy RTOS knows who to talk to
    if(const auto ret = resetDriver(s, dest); ret < 0)
    {
        std::cerr << "Failed to reset driver: " << returnValueToErrorCode(ret).message() << std::endl;
        return -1;
    }

    // Initialize the driver
    {
        flr_api::v2::rtos::JointLimits ext_axis_limits;
        ext_axis_limits.set_min_position(0);
        ext_axis_limits.set_max_position(1000);
        ext_axis_limits.set_max_velocity(1000);
        ext_axis_limits.set_max_acceleration(5000);
        ext_axis_limits.set_max_jerk(10000);

        flr_api::v2::rtos::ExtendedAxisDefinition ext_axis_def;
        *ext_axis_def.mutable_joint_limits() = ext_axis_limits;

        flr_api::v2::rtos::RTOSDriverInitializationCmd rtos_init_cmd;
        rtos_init_cmd.set_dummy_mode(false);
        rtos_init_cmd.set_robot_brand("Fanuc");
        rtos_init_cmd.set_robot_model("LR Mate 200iD/7L");
        rtos_init_cmd.set_robot_controller_model("R-30iB-Plus");
        *rtos_init_cmd.add_extended_axes() = ext_axis_def;
        rtos_init_cmd.set_local_address("127.0.0.1");
        rtos_init_cmd.set_robot_address("127.0.0.1");

        std::cout << "Sending initialization message\n";
        const auto payload = asAny(rtos_init_cmd).SerializeAsString();
        if(const auto ret = sendto(s, payload.data(), static_cast<int>(payload.size()), 0, reinterpret_cast<sockaddr*>(&dest), sizeof(dest)); ret < 0)
        {
            std::cerr << "Failed to send: " << returnValueToErrorCode(ret).message() << std::endl;
            return -1;
        }
    }

    std::vector<double> current_cmd;

    const auto pos1 = std::make_pair<std::vector<double>, std::vector<double>>({1.57079632679, 0., 0., 1.57079632679, 0., 0.}, {300});
    const auto pos2 = std::make_pair<std::vector<double>, std::vector<double>>({0., 0., 0., 0., 0., 0.}, {500});
    const auto pos3 = std::make_pair<std::vector<double>, std::vector<double>>({-1.57079632679, 0., 0., 1.57079632679, 0., 0.}, {700});

    const auto pos_array = std::array{pos2, pos1, pos3, pos2};
    std::size_t pos_idx{0};

    auto t_start = std::chrono::steady_clock::now();

    auto scenario = EScenario::RobotOnly;

    while(keep_running)
    {
        flr_api::v2::rtos::SystemState sys_state_msg;
        sockaddr from{};
        socklen_t from_sz = sizeof(from);
        if(int ret = recvfrom(s, recv_data_buf.data(), static_cast<int>(recv_data_buf.size()), 0, &from, &from_sz); ret < 0)
        {
            std::cerr << "Failed to receive: " << returnValueToErrorCode(ret).message() << std::endl;
            break;
        }
        else
        {
            if(!sys_state_msg.ParseFromArray(recv_data_buf.data(), ret))
            {
                std::cerr << "Failed to parse message\n";
                break;
            }
        }

        if(sys_state_msg.rtos_state().robot_control_mode() != flr_api::v2::rtos::RTOSControlMode::RTOS_CONTROL_MODE_JOGGING)
        {
            flr_api::v2::rtos::RTOSControlModeCmd jog_mode_msg;
            jog_mode_msg.set_control_mode_request(flr_api::v2::rtos::RTOSControlMode::RTOS_CONTROL_MODE_JOGGING);
            const auto payload = asAny(jog_mode_msg).SerializeAsString();
            if(const auto ret = sendto(s, payload.data(), static_cast<int>(payload.size()), 0, reinterpret_cast<sockaddr*>(&dest), sizeof(dest)); ret < 0)
            {
                std::cerr << "Failed to send jog mode: " << returnValueToErrorCode(ret).message() << std::endl;
                break;
            }
            continue;
        }

        current_cmd.clear();
        for(auto i = 0; i < sys_state_msg.rtos_state().joint_position_setpoints().value_size(); ++i)
        {
            current_cmd.push_back(sys_state_msg.rtos_state().joint_position_setpoints().value(i));
        }
        for(auto i = 0; i < sys_state_msg.rtos_state().extended_joint_position_setpoints().value_size(); ++i)
        {
            current_cmd.push_back(sys_state_msg.rtos_state().extended_joint_position_setpoints().value(i));
        }

        if(std::chrono::steady_clock::now() - t_start > std::chrono::seconds(10))
        {
            ++pos_idx;
            t_start = std::chrono::steady_clock::now();

            if(pos_idx == pos_array.size())
            {
                pos_idx = 0;
                std::cout << "Switching scenario\n";
                scenario = static_cast<EScenario>((static_cast<std::underlying_type_t<EScenario>>(scenario) + 1) % 3);
            }
            else
            {
                std::cout << "Switching position\n";
            }
        }

        flr_api::v2::rtos::JointPositionJoggingCmd jog_cmd_msg;
        if(scenario == EScenario::RobotOnly || scenario == EScenario::CombinedAxes)
        {
            for(double q : pos_array.at(pos_idx).first)
            {
                jog_cmd_msg.mutable_joint_positions()->add_value(q);
            }
        }
        if(scenario == EScenario::ExtAxisOnly || scenario == EScenario::CombinedAxes)
        {
            for(double q : pos_array.at(pos_idx).second)
            {
                jog_cmd_msg.mutable_extended_joint_positions()->add_value(q);
            }
        }
        const auto payload = asAny(jog_cmd_msg).SerializeAsString();
        if(const auto ret = sendto(s, payload.data(), static_cast<int>(payload.size()), 0, reinterpret_cast<sockaddr*>(&dest), sizeof(dest)); ret < 0)
        {
            std::cerr << "Failed to send jog command: " << returnValueToErrorCode(ret).message() << std::endl;
            break;
        }
    }

    // Reset the driver
    if(const auto ret = resetDriver(s, dest); ret < 0)
    {
        std::cerr << "Failed to reset driver: " << returnValueToErrorCode(ret).message() << std::endl;
        return -1;
    }

#if defined(_WIN32)
    closesocket(s);
    WSACleanup();
#else
    close(s);
#endif

    return 0;
}
